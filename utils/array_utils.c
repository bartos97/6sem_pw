#include "array_utils.h"

void fillArrayWithRandom(ARRAY_TYPE* const arrayStart, const size_t arrayCount, int maxValue, time_t seed)
{
    srand(seed);
    maxValue = maxValue == 0 ? RAND_MAX : maxValue;
    const int isNegativeOnly = maxValue < 0 ? 1 : 0;
    int sign;
    double decimal;

    for (size_t i = 0; i < arrayCount; i++)
    {
        sign = isNegativeOnly || rand() > RAND_MAX / 2 ? -1 : 1;
        decimal = rand() / (ARRAY_TYPE)RAND_MAX;
        arrayStart[i] = (ARRAY_TYPE)(sign * (rand() % maxValue)) + (ARRAY_TYPE)decimal;
    }
}


void printArray(ARRAY_TYPE* const arrayStart, const size_t arrayCount)
{
    for (size_t i = 0; i < arrayCount; i++)
    {
        printf(PRINT_FULL_FORMAT" ", arrayStart[i]);
        fflush(stdout);
    }
    printf("\n");
}


void printFlattenArray(ARRAY_TYPE* const arrayStart, const size_t arrayCount, size_t rowCount, size_t colCount)
{
    for (size_t row = 0; row < rowCount; row++)
    {
        putchar('|');
        for (size_t col = 0; col < colCount; col++)
        {
            printf(PRINT_FULL_FORMAT" ", arrayStart[row *colCount+ col]);
        }
        printf("|\n");
    }
}


void printFlattenArrayWolframNotation(ARRAY_TYPE* const arrayStart, const size_t arrayCount, size_t rowCount, size_t colCount)
{
    putchar('{');
    for (size_t row = 0; row < rowCount; row++)
    {
        if (row == 0) putchar('{');
        else printf(", {");
        for (size_t col = 0; col < colCount; col++)
        {
            if (col == 0) printf("%"STRINGIFY(PRINT_SPCIFIER), arrayStart[row *colCount+ col]);
            else printf(", %"STRINGIFY(PRINT_SPCIFIER), arrayStart[row *colCount+ col]);
        }
        putchar('}');
    }
    printf("}\n");
}


void printFlattenArrayToFile(ARRAY_TYPE* const arrayStart, const size_t arrayCount, size_t rowCount, size_t colCount, FILE* file)
{
    for (size_t row = 0; row < rowCount; row++)
    {
        putc('|', file);
        for (size_t col = 0; col < colCount; col++)
        {
            fprintf(file, PRINT_FULL_FORMAT, arrayStart[row *colCount+ col]);
        }
        fprintf(file, "|\n");
    }
}


void printFlattenArrayToFileWolframNotation(ARRAY_TYPE* const arrayStart, const size_t arrayCount, size_t rowCount, size_t colCount, FILE* file)
{
    putc('{', file);
    for (size_t row = 0; row < rowCount; row++)
    {
        if (row == 0) putc('{', file);
        else fprintf(file, ", {");
        for (size_t col = 0; col < colCount; col++)
        {
            if (col == 0) fprintf(file, "%"STRINGIFY(PRINT_SPCIFIER), arrayStart[row *colCount+ col]);
            else fprintf(file, ", %"STRINGIFY(PRINT_SPCIFIER), arrayStart[row *colCount+ col]);
        }
        putc('}', file);
    }
    fprintf(file, "}");
}


void printArrayToFile(ARRAY_TYPE* const arrayStart, const size_t arrayCount, FILE* file)
{
    for (size_t i = 0; i < arrayCount; i++)
    {
        fprintf(file, "%"STRINGIFY(PRINT_SPECIFIER)"\n", arrayStart[i]);
    }
}


void copyArray(ARRAY_TYPE* arraySource, const size_t arraySourceCount, ARRAY_TYPE* arrayDest, const size_t arrayDestCount)
{
    const size_t smallerCount = arraySourceCount <= arrayDestCount ? arraySourceCount : arrayDestCount;
    for (size_t i = 0; i < smallerCount; i++)
    {
        arrayDest[i] = arraySource[i];
    }    
}
