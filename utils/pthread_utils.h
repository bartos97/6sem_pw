#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "errors.h"

static void waitForThread(pthread_t ID)
{
    checkError(pthread_join(ID, NULL), "waitForThread");
#ifndef SILENT_JOIN
    printf("Joined  thread #%ld\n", ID);
#endif
}