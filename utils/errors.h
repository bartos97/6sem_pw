#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#define handle_error_en(en, msg) \
    do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#ifdef RELEASE_MODE
    #define CUSTOM_LOG(...)
    #define CUSTOM_ASSERT(x, ...)
#else
    #define CUSTOM_LOG(...) \
    printf("%s at line %d: ", __FILE__, __LINE__); \
    printf(__VA_ARGS__); \
    putchar('\n'); \

    #define CUSTOM_ASSERT(x, ...) \
    if (!(x)) \
    {\
        printf("ASSERTION FAILED!:\n"); \
        CUSTOM_LOG(__VA_ARGS__); \
        exit(EXIT_FAILURE); \
    }
#endif

static void checkError(int retCode, const char* msg)
{
#ifndef RELEASE_MODE
    if (retCode)
    {
        printf("%d ", retCode);
        handle_error_en(retCode, msg);
    }
#endif
}

static void checkErrorErrno(int retCode, const char* msg)
{
#ifndef RELEASE_MODE
    if (retCode == -1)
    {
        printf("%d ", retCode);
        handle_error_en(retCode, msg);
    }
#endif
}