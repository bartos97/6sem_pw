#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define _STRINGIFY(s) #s
#define STRINGIFY(s) _STRINGIFY(s)

#define ARRAY_TYPE double
#define PRINT_SPCIFIER lf
#define PRINT_WIDTH 
#define PRINT_PRECISION .3

#define PRINT_FULL_FORMAT "%"STRINGIFY(PRINT_WIDTH)STRINGIFY(PRINT_PRECISION)STRINGIFY(PRINT_SPCIFIER)

/**
 * @param arrayStart pointer to beginning of array
 * @param arrayCount number of elements in array
 * @param maxValue maximum random value to be in array
 */
void fillArrayWithRandom(ARRAY_TYPE* const arrayStart, const size_t arrayCount, int maxValue, time_t seed);

/**
 * @param arrayStart pointer to beginning of array
 * @param arrayCount number of elements in array
 */
void printArray(ARRAY_TYPE* const arrayStart, const size_t arrayCount);

/**
 * @param arrayStart pointer to beginning of flatten 2D array
 * @param arrayCount number of elements in array
 * @param rowCount number of rows in array
 * @param colCount number of columns in array
 */
void printFlattenArray(ARRAY_TYPE* const arrayStart, const size_t arrayCount, size_t rowCount, size_t colCount);
void printFlattenArrayWolframNotation(ARRAY_TYPE* const arrayStart, const size_t arrayCount, size_t rowCount, size_t colCount);

/**
 * @param arrayStart pointer to beginning of flatten 2D array
 * @param arrayCount number of elements in array
 * @param rowCount number of rows in array
 * @param colCount number of columns in array
 * @param file pointer to FILE struct, has to be opened for writing
 */
void printFlattenArrayToFile(ARRAY_TYPE* const arrayStart, const size_t arrayCount, size_t rowCount, size_t colCount, FILE* file);
void printFlattenArrayToFileWolframNotation(ARRAY_TYPE* const arrayStart, const size_t arrayCount, size_t rowCount, size_t colCount, FILE* file);

/**
 * @param arrayStart pointer to beginning of array
 * @param arrayCount number of elements in array
 * @param file pointer to FILE struct, has to be opened for writing
 */
void printArrayToFile(ARRAY_TYPE* const arrayStart, const size_t arrayCount, FILE* file);

/**
 * @param arrayStart pointer to beginning of source array
 * @param arrayStartCount number of elements in source array
 * @param arrayDest pointer to beginning of destination array
 * @param arrayDestCount number of elements in destination array
 */
void copyArray(ARRAY_TYPE* arraySource, const size_t arraySourceCount, ARRAY_TYPE* arrayDest, const size_t arrayDestCount);
