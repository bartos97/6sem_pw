package utils;

import java.util.List;
import java.util.Random;
import java.io.FileWriter;
import java.io.IOException;

public class ArrayUtils {
    /**
     * Wypisuje podaną listę w postaci [x1, x1, ..., xN] na standardowe wyjście
     * @param <T> typ elementów listy
     * @param vector lista do wypisania
     */
    public static <T> void print(final List<T> vector) {
        System.out.print("[");
        for (int i = 0; i < vector.size() - 1; i++) {
            System.out.print(vector.get(i));
            System.out.print(", ");
        }
        System.out.print(vector.get(vector.size() - 1));
        System.out.println("]");
    }

    /**
     * Wypisuje podaną listę na standardowe wyjście w postaci:
     * [[x11, x12, ..., x1N]
     *  [x21, x22, ..., x2N]
     *  . . .
     *  [xN1, xN2, ..., xNN]]
     * @param <T> typ elementów listy
     * @param matrix lista 2D (lista list) do wypisania
     */
    public static <T> void print2D(final List<List<T>> matrix) {
        System.out.print("[");
        for (List<T> vec : matrix) {
            System.out.print(' ');
            print(vec);
            System.out.println(", ");
        }
        System.out.println("]");
    }

    /**
     * Wypisuje podaną tablicę 2D na standardowe wyjście w postaci:
     * [[x11, x12, ..., x1N]
     *  [x21, x22, ..., x2N]
     *  . . .
     *  [xN1, xN2, ..., xNN]]
     * @param matrix tablica 2D do wypisania
     */
    public static void print2D(final char[][] matrix) {
        System.out.print("[");
        for (char[] row : matrix) {
            System.out.print(" [");
            for (int i = 0; i < row.length - 1; i++) {
                System.out.print(row[i]);
                System.out.print(", ");
            }
            System.out.print(row[row.length - 1]);
            System.out.println("], ");
        }
        System.out.println("]");
    }

    /**
     * Zapisuje do podanego pliku podaną tablicę 2D w postaci:
     * x11, x12, ..., x1N
     * x21, x22, ..., x2N
     * . . .
     * xN1, xN2, ..., xNN
     * @param matrix tablica 2D do zapisania
     * @param filepath ścieżka do pliku wynikowego
     */
    public static void printToFile2D(final char[][] matrix, String filepath) {
        try {
            FileWriter myWriter = new FileWriter(filepath);
            
            for (char[] row : matrix) {
                for (int i = 0; i < row.length - 1; i++) {
                    myWriter.write(row[i]);
                    myWriter.write(" ");
                }
                myWriter.write(row[row.length - 1]);
                myWriter.write("\n");
            }

            myWriter.close();
            System.out.println("Successfully wrote char image to the file " + filepath);
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    /**
     * Wypełnia podaną tablicę podaną wartością
     * @param vector tablica do której zapisane zostaną dane
     * @param length ile losowych elementów umieścić w liście
     * @param value wartość którą wypełnić tablicę
     */
    public static void fillWithValue(final List<Integer> vector, int length, Integer value) {
        vector.clear();
        for (int i = 0; i < length; i++) {
            vector.add(value);
        }
    }

    /**
     * Wypełnia podaną tablicę losowymi liczbami całkowitymi
     * @param vector tablica do której zapisane zostaną dane
     * @param length ile losowych elementów umieścić w liście
     */
    public static void fillWithRandom(final List<Integer> vector, int length) {
        fillWithRandom(vector, length, Integer.MAX_VALUE);
    }

    /**
     * Wypełnia podaną tablicę losowymi liczbami całkowitymi z przedziału [0, {@code upperBound})
     * @param vector tablica do której zapisane zostaną dane
     * @param length ile losowych elementów umieścić w liście
     * @param upperBound górny limit liczb losowych
     */
    public static void fillWithRandom(final List<Integer> vector, int length, final int upperBound) {
        final var randGenerator = new Random();
        fillWithRandom(vector, length, upperBound, randGenerator);
    }

    /**
     * Wypełnia podaną tablicę losowymi liczbami całkowitymi z przedziału [0, {@code upperBound}) używając generatora zainicjowanego ziarnem {@code seed}
     * @param vector tablica do której zapisane zostaną dane
     * @param length ile losowych elementów umieścić w liście
     * @param upperBound górny limit liczb losowych
     * @param seed ziarno generatora liczb losowych
     */
    public static void fillWithRandom(final List<Integer> vector, int length, final int upperBound, final long seed) {
        final var randGenerator = new Random(seed);
        fillWithRandom(vector, length, upperBound, randGenerator);
    }

    /**
     * Wypełnia podaną tablicę losowymi liczbami całkowitymi z przedziału [0, {@code upperBound}) używając podanego generatora
     * @param vector tablica do której zapisane zostaną dane
     * @param length ile losowych elementów umieścić w liście
     * @param upperBound górny limit liczb losowych
     * @param generator generator liczb losowych
     */
    public static void fillWithRandom(final List<Integer> vector, int length, final int upperBound, Random generator) {
        vector.clear();
        for (int i = 0; i < length; i++) {
            vector.add(generator.nextInt(upperBound));
        }
    }
}