package utils;

public class ThreadUtils {
    
    /**
     * Wstrzymuje wątek w którym zostanie wywołane do czasu przyłączenia wszystkich wątków. Przechwytuje ewentualne wyjątki
     * @param threads tablica wątków do przyłączenia
     */
    public static void waitForThreads(Thread[] threads) {
        for (int i = 0; i < threads.length; i++) {
            waitForThread(threads[i]);
        }
    }

    /**
     * Wstrzymuje wątek w którym zostanie wywołane do czasu przyłączenia wskazanego wątku. Przechwytuje ewentualne wyjątki
     * @param thread wątek do przyłączenia
     */
    public static void waitForThread(Thread thread) {
        try {
            thread.join();
        } catch (final InterruptedException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}