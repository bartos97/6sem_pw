package lab9.zad3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

public class Main {
    private static boolean[] sourceArray;

    public static void main(String[] args) {
        final int ARRAY_COUNT = args.length > 0 ? Integer.parseInt(args[0]) : 50;
        final int NUM_OF_CPUS = Runtime.getRuntime().availableProcessors();

        if (ARRAY_COUNT < 4) {
            System.out.println("Invalid argument");
            return;
        }

        sourceArray = new boolean[ARRAY_COUNT];
        Arrays.fill(sourceArray, true);

        long timeStart = System.nanoTime();
        runSieve(NUM_OF_CPUS);
        double timeTotal = (System.nanoTime() - timeStart) / 1e9;

        System.out.println("Calculation time: " + timeTotal + "s");
        saveResultToFile("prime_numbers.dat");
    }

    /**
     * uruchamia procedurę sita na równomiernie rozłożonych przedziałach między wątki
     * @param numOfThreads maksymalna liczba wątków na ilu spróbować uruchomić procedurę sita
     */
    private static void runSieve(int numOfThreads) {
        while (numOfThreads >= (sourceArray.length - 2) / 2) {
            numOfThreads /= 2;
        }
        if (numOfThreads < 2) {
            numOfThreads = 1;
        }

        Thread[] threads = new PrimesSieve[numOfThreads];
        final int numsPerThread = (sourceArray.length - 2) / numOfThreads;
        int leftoverChunkCount = (sourceArray.length - 2) % numOfThreads;
        int startIndex = 2;

        for (int i = 0; i < numOfThreads; i++) {
            int tmpEndIndex = startIndex + numsPerThread;
            if (leftoverChunkCount > 0) {
                tmpEndIndex++;
                leftoverChunkCount--;
            }
            threads[i] = new PrimesSieve(sourceArray, startIndex, tmpEndIndex);
            threads[i].start();
            startIndex = tmpEndIndex;
        }

        for (int i = 0; i < numOfThreads; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /**
     * zapisuje do pliku znalezione liczby pierwsze, w jednej linii, odzielone spacją
     * @param filepath ścieżka do pliku w którym zapisany zostanie wynik
     */
    private static void saveResultToFile(String filepath) {
        try {
            FileWriter myWriter = new FileWriter(filepath);            
            for (int i = 2; i < sourceArray.length; i++) {
                if (sourceArray[i]) {
                    myWriter.write(String.format("%d ", i));
                }
            }
            myWriter.close();
            System.out.println("Successfully wrote result to the file " + filepath);
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}