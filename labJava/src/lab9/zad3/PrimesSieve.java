package lab9.zad3;

public class PrimesSieve extends Thread {
    /**
     * 
     * @param data tablica reprezentująca wykreślone liczby naturalne; data[i] == false => liczba i wykreślona => liczba i nie jest liczbą pierwszą
     * @param startIndex początek przedziału z którego wykreślać liczby (włącznie)
     * @param pastEndIndex koniec przedziału z którego wykreślać liczby (wyłącznie)
     */
    public PrimesSieve(boolean[] data, int startIndex, int pastEndIndex) {
        this.ID = idCounter++;
        this.startIndex = startIndex;
        this.pastEndIndex = pastEndIndex;
        this.data = data;

        System.out.println("Thread #" + this.ID + " starts looking in range: [" + this.startIndex + ", " + this.pastEndIndex + ")");
    }

    /**
     * wykreślna liczby które nie są pierwsze w przekazanej tablicy w przekazanym przedziale
     */
    @Override
    public void run() {
        for (int i = 2; i < this.data.length; i++) {
            if (this.data[i] == false) { //wykreslone
                continue;
            }

            //znaleziona liczba pierwsza; przeszukaj zakres i wykreśl wielokrotności
            for (int j = this.startIndex; j < this.pastEndIndex; j++) {
                if (i == j) {
                    continue;
                }
                if ((j % i) == 0) { // j jest wielokrotnością i
                    synchronized(this.data) {
                        this.data[j] = false;
                    }
                }
            }
        }
    }

    private static int idCounter = 0;
    private final int ID;
    private final int startIndex;
    private final int pastEndIndex;
    private boolean[] data;
}