package lab9.zad1;

import java.util.ArrayList;
import java.util.List;

import utils.*;

class VectorAdder extends Thread {
    /**
     * 
     * @param startIndex indeks w wektorach od którego zacząć sumowanie (włącznie)
     * @param pastEndIndex indeks w wektorach na których zakończyć sumowanie (wyłącznie)
     * @param vec1 pierwszy wektor do zsumowania
     * @param vec2 drugi wektor do zsumowania
     * @param vecResult wektor do którego zapisać wynik
     */
    public VectorAdder(int startIndex, int pastEndIndex, List<Integer> vec1, List<Integer> vec2, List<Integer> vecResult) {
        this.id = counter++;
        this.startIndex = startIndex;
        this.pastEndIndex = pastEndIndex;
        this.vec1 = vec1;
        this.vec2 = vec2;
        this.vecResult = vecResult;

        System.out.printf("Thread #%d created from %d to %d\n", this.id, this.startIndex, this.pastEndIndex - 1);
    }

    /**
     * Sumuje przekazane w konstruktorze wektory do przekazanego wektora wynikowego w przekazany zakresie
     */
    @Override
	public void run() {
		for (int i = this.startIndex; i < this.pastEndIndex; i++) {
            this.vecResult.set(i, this.vec1.get(i) + this.vec2.get(i));
        }
	}
    
    private static long counter = 0;
    private final long id;
    private final int startIndex;
    private final int pastEndIndex;
    private final List<Integer> vec1;
    private final List<Integer> vec2;
    private final List<Integer> vecResult;
}

public class Main {
    public static void main(final String[] args) {
        final int VECTORS_COUNT = args.length > 0 ? Integer.parseInt(args[0]) : 10;
        final int NUM_OF_THREADS = args.length > 1 ? Integer.parseInt(args[1]) : 2;
        final int RAND_MAX = args.length > 2 ? Integer.parseInt(args[2]) : 10;
        // final int SEED = args.length > 3 ? Integer.parseInt(args[3]) : 0;

        if (NUM_OF_THREADS > VECTORS_COUNT / 2) {
            System.out.println("Invalid program input");
            return;
        }
        
        final var sourceVector1 = new ArrayList<Integer>(VECTORS_COUNT);
        final var sourceVector2 = new ArrayList<Integer>(VECTORS_COUNT);
        final var resultVector = new ArrayList<Integer>(VECTORS_COUNT);

        ArrayUtils.fillWithRandom(sourceVector1, VECTORS_COUNT, RAND_MAX);
        ArrayUtils.fillWithRandom(sourceVector2, VECTORS_COUNT, RAND_MAX);
        ArrayUtils.fillWithValue(resultVector, VECTORS_COUNT, 0);

        System.out.print("Vector 1: ");
        ArrayUtils.print(sourceVector1);
        System.out.print("Vector 2: ");
        ArrayUtils.print(sourceVector2);

        var threads = new VectorAdder[NUM_OF_THREADS];
        final int perThreadChunkCount = VECTORS_COUNT / NUM_OF_THREADS;
        int leftoverChunkCount = VECTORS_COUNT % NUM_OF_THREADS;
        int currentIndex = 0;

        for (int i = 0; i < NUM_OF_THREADS; i++) {
            int tmpEndIndex = currentIndex + perThreadChunkCount;

            if (leftoverChunkCount > 0) {
                tmpEndIndex++;
                threads[i] = new VectorAdder(currentIndex, tmpEndIndex, sourceVector1, sourceVector2, resultVector);
                threads[i].start();
                leftoverChunkCount--;
            }
            else {
                threads[i] = new VectorAdder(currentIndex, tmpEndIndex, sourceVector1, sourceVector2, resultVector);
                threads[i].start();
            }

            currentIndex = tmpEndIndex;
        }

        ThreadUtils.waitForThreads(threads);

        System.out.print("Vector R: ");
        ArrayUtils.print(resultVector);
    }
}