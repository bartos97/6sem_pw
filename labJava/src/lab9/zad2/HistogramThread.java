package lab9.zad2;

import java.util.HashMap;
import java.util.Map;

class HistogramThread extends Thread {
    public HistogramThread(int imgWidth, int imgHeight, char[][] imgData, char valueToFind) {
        this(imgWidth, imgHeight, imgData, new char[]{valueToFind});
    }

    /**
     * 
     * @param imgWidth szerokość obrazu
     * @param imgHeight wysokość obrazu
     * @param imgData dane obrazu
     * @param valuesToFind jakich znaków szukać
     */
    public HistogramThread(int imgWidth, int imgHeight, char[][] imgData, char[] valuesToFind) {
        this.ID = idCounter++;
        this.imgWidth = imgWidth;
        this.imgHeight = imgHeight;
        this.imgData = imgData;
        this.valuesToFind = valuesToFind;
        this.histogram = new HashMap<>(valuesToFind.length);

        printSelf();
    }

    /**
     * przeszukuje przekazany obraz i zlicza wystąpienia przekazanych znaków
     */
    @Override
    public void run() {
        for (char c : this.valuesToFind) {
            int valCounter = 0;
            for (int i = 0; i < this.imgHeight; i++) {
                for (int j = 0; j < this.imgWidth; j++) {
                    if (this.imgData[i][j] == c) {
                        ++valCounter;
                    }
                }
            }
            this.histogram.put(c, valCounter);
        }
    }

    /**
     * 
     * @return mapa odwzorowująca dany znak na ilość jego wystąpień w obrazie
     */
    public Map<Character, Integer> getHistogram() {
        return histogram;
    }

    /**
     * wypisuje wewnętrzen ID wątku oraz jakich znaków będzie szukał
     */
    private void printSelf() {
        System.out.print("Thread #" + this.ID + " starts looking for: ");
        for (char c : valuesToFind) {
            System.out.print(c + " ");
        }
        System.out.printf("\n");
    }

    private static int idCounter = 0;
    private final int ID;
    private int imgWidth;
    private int imgHeight;
    private char[][] imgData;
    private char[] valuesToFind;
    private Map<Character, Integer> histogram;
}
