package lab9.zad2;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import utils.*;

/**
 * Klasa reprezentująca "obraz" jako dwuwymiarową tablicę znaków
 */
public class CharImage {
    public static final int DEFAULT_IMG_WIDTH = 10;
    public static final int DEFAULT_IMG_HEIGHT = 10;
    public static final int DEFAULT_DATA_LOWER_BOUND = 33;
    public static final int DEFAULT_DATA_UPPER_BOUND = 127;

    /**
     * Tworzy obraz o domyślnych wymiarach z losowo wygenerowanych znaków ASCII z domyślnego zakresu
     */
    public CharImage() {
        this(DEFAULT_IMG_WIDTH, DEFAULT_IMG_HEIGHT);
    }
    
    /**
     * Tworzy obraz o podanych wymiarach z losowo wygenerowanych znaków ASCII z domyślnego zakresu
     * @param width szerokość obrazu
     * @param height wysokość obrazu
     */
    public CharImage(int width, int height) {
        this(width, height, DEFAULT_DATA_LOWER_BOUND, DEFAULT_DATA_UPPER_BOUND);
    }

    /**
     * Tworzy obraz o podanych wymiarach z losowo wygenerowanych znaków ASCII z podanego zakresu
     * @param width szerokość obrazu
     * @param height wysokość obrazu
     * @param dataLowerBound dolny limit zakresu znaków ASCII z których zostanie wygenerowany obraz
     * @param dataUpperBound górny limit zakresu znaków ASCII z których zostanie wygenerowany obraz
     */
    public CharImage(int width, int height, int dataLowerBound, int dataUpperBound) {
        this.width = width;
        this.height = height;
        this.data = new char[height][width];
        this.dataLowerBound = dataLowerBound;
        this.dataUpperBound = dataUpperBound;
        this.histogram = new HashMap<>(dataUpperBound - dataLowerBound);
        
        generateImage();
    }

    /**
     * Oblicza histogram obrazu wielowątkowo
     * Ilość wątków roboczych jest równa ilości dostępnych procesorów w systemie 
     * lub ilości możliwych znaków w obrazie jeśli mniejsza nić ilość CPU
     * Zakresy znaków do przeszukania są równomiernie rozdystrybuowane między wątki
     */
    public void calculateHistogram() {
        final int numOfPossibleChars = this.dataUpperBound - this.dataLowerBound;
        final int cores = Runtime.getRuntime().availableProcessors();
        final int numOfThreads = cores < numOfPossibleChars ? cores : numOfPossibleChars;

        HistogramThread[] threads = new HistogramThread[numOfThreads];
        final int charsPerThread = numOfPossibleChars / numOfThreads;
        int leftoverCharsCount = numOfPossibleChars % numOfThreads;
        int startIndex = this.dataLowerBound;

        for (int i = 0; i < numOfThreads; i++) {
            int tmpEndIndex = startIndex + charsPerThread;

            if (leftoverCharsCount > 0) {
                tmpEndIndex++;
                leftoverCharsCount--;
            }

            char[] charsToFind = this.getCharsToFind(startIndex, tmpEndIndex);
            threads[i] = new HistogramThread(this.width, this.height, this.data, charsToFind);
            threads[i].start();

            startIndex = tmpEndIndex;
        }

        for (int i = 0; i < numOfThreads; i++) {
            try {
                threads[i].join();
                this.histogram.putAll(threads[i].getHistogram());
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Wypisuje na standardowe wyjście tablice 2D znaków reprezentującą obraz
     */
    public void print() {
        ArrayUtils.print2D(this.data);
    }

    /**
     * Wypisuje do pliku tablice 2D znaków reprezentującą obraz
     * @param filepath ścieżka do pliku w którym zapisać dane
     */
    public void printToFile(String filepath) {
        ArrayUtils.printToFile2D(this.data, filepath);
    }

    /**
     * Wypisuje na standardowe wyjście obliczony histogram w postaci:
     * ZNAK x ILOŚĆ WYSTAPIEŃ
     */
    public void printHistogram() {
        for(Map.Entry<Character, Integer> entry : this.histogram.entrySet()) {
            Character key = entry.getKey();
            Integer value = entry.getValue();

            if (value == 0) continue;
        
            System.out.printf("%c x %d", key, value);
            System.out.printf("\n");
        }
    }

    /**
     * Wypisuje do podanego pliku obliczony histogram w postaci:
     * ZNAK x ILOŚĆ WYSTAPIEŃ
     * @param filepath ścieżka do pliku w którym zapisać dane
     */
    public void printHistogramToFile(String filepath) {
        try {
            FileWriter myWriter = new FileWriter(filepath);
            
            for(Map.Entry<Character, Integer> entry : this.histogram.entrySet()) {
                Character key = entry.getKey();
                Integer value = entry.getValue();
    
                if (value == 0) continue;
            
                myWriter.write(String.format("%c x %d", key, value));
                myWriter.write("\n");
            }

            myWriter.close();
            System.out.println("Successfully wrote histogram result to the file " + filepath);
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    /**
     * Generuje obraz za pomocą znaków ASCII z zakresu podanego w konstruktorze
     */
    private void generateImage() {
        final var generator = new Random();
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                data[i][j] = (char)(generator.nextInt(this.dataUpperBound - this.dataLowerBound) + this.dataLowerBound);
            }
        }
    }

    /**
     * Zwraca tablicę znaków ASCII z podanego zakresu: [startIndex, endIndex)
     * @param startIndex początek zakresu (włącznie)
     * @param endIndex koniec zakresu (wyłącznie)
     * @return tablica znaków ASCII
     */
    private char[] getCharsToFind(int startIndex, int endIndex) {
        var chars = new char[endIndex - startIndex];
        int charIndex = startIndex;

        for (int i = 0; i < chars.length; i++) {
            chars[i] = (char)charIndex;
            charIndex++;
        }

        return chars;
    }

    private int width;
    private int height;
    private char[][] data;
    private int dataLowerBound;
    private int dataUpperBound;
    private Map<Character, Integer> histogram;
}