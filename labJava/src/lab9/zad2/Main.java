package lab9.zad2;

public class Main {
    public static void main(String[] args) {
        final int IMG_WIDTH = args.length > 0 ? Integer.parseInt(args[0]) : CharImage.DEFAULT_IMG_WIDTH;
        final int IMG_HEIGHT = args.length > 1 ? Integer.parseInt(args[1]) : CharImage.DEFAULT_IMG_HEIGHT;
        final int ASCII_START = args.length > 2 ? Integer.parseInt(args[2]) : CharImage.DEFAULT_DATA_LOWER_BOUND;
        final int ASCII_END = args.length > 3 ? Integer.parseInt(args[3]) : CharImage.DEFAULT_DATA_UPPER_BOUND;

        var img = new CharImage(IMG_WIDTH, IMG_HEIGHT, ASCII_START, ASCII_END);
        img.printToFile("char_image.dat");

        long timeStart = System.nanoTime();
        img.calculateHistogram();
        double timeTotal = (System.nanoTime() - timeStart) / 1e9;

        img.printHistogramToFile("char_image_histogram.dat");
        System.out.println("Histogram calculation time: " + timeTotal + "s");
    }
}