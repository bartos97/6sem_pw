#include <iostream>
#include <mpi.h>
#include "./GrayScaleImage.hpp"


int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);
    srand(time(nullptr));
    std::locale::global(std::locale(""));
    std::cout.imbue(std::locale());

    const size_t LOOPS_AMOUNT = argc > 1 ? atol(argv[1]) > 0l ? atol(argv[1]) : 1 : 1;

    int numOfProcesses, selfRank;
    MPI_Comm_size(MPI_COMM_WORLD, &numOfProcesses);
    MPI_Comm_rank(MPI_COMM_WORLD, &selfRank);

    if (numOfProcesses != 2)
    {
        std::cerr << "Invalid processes amount, 2 required" << std::endl;
        exit(EXIT_FAILURE);
    }

    const int blocklengths[] = {1, 1, 1, GrayScaleImage::MAX_FILENAME_LENGTH, GrayScaleImage::MAX_HEIGHT * GrayScaleImage::MAX_WIDTH};
    const MPI_Aint offsets[] = {
        offsetof(GrayScaleImage, ID),
        offsetof(GrayScaleImage, width),
        offsetof(GrayScaleImage, height),
        offsetof(GrayScaleImage, fileName),
        offsetof(GrayScaleImage, data)
    };
    const MPI_Datatype types[] = {MPI_UNSIGNED, MPI_UNSIGNED, MPI_UNSIGNED, MPI_CHAR, MPI_UNSIGNED_CHAR};
    MPI_Datatype grayScaleImgMpiType;
    MPI_Type_create_struct(5, blocklengths, offsets, types, &grayScaleImgMpiType);
    MPI_Type_commit(&grayScaleImgMpiType);

    unsigned imgW, imgH;
#ifdef RELEASE_MODE
    imgW = GrayScaleImage::MAX_WIDTH;
    imgH = GrayScaleImage::MAX_HEIGHT;
#else
    imgW = 4;
    imgH = 2;
#endif

    char* packedDataBuffer = new char[LOOPS_AMOUNT * sizeof(GrayScaleImage)];
    GrayScaleImage* outputImgs = new GrayScaleImage[LOOPS_AMOUNT];
    double timeStart = MPI_Wtime();

    if (selfRank == 0)
    {
        int position = 0;
        GrayScaleImage inputImg{"input_file", imgW, imgH};
        inputImg.createRandomData();

    #ifndef RELEASE_MODE
        std::cout << "Data to sent\n" << inputImg;
    #endif

        for (size_t i = 0; i < LOOPS_AMOUNT; i++)
        {
            MPI_Pack(&inputImg, 1, grayScaleImgMpiType, packedDataBuffer, LOOPS_AMOUNT * sizeof(GrayScaleImage), &position, MPI_COMM_WORLD);
        }

    #ifdef SEND_SYNC
        MPI_Ssend(packedDataBuffer, position, MPI_PACKED, 1, 13, MPI_COMM_WORLD);
    #else
        MPI_Send(packedDataBuffer, position, MPI_PACKED, 1, 13, MPI_COMM_WORLD);
    #endif
    }

    else if (selfRank == 1)
    {
        MPI_Recv(outputImgs, LOOPS_AMOUNT, grayScaleImgMpiType, 0, 13, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    #ifndef RELEASE_MODE
        for (size_t i = 0; i < LOOPS_AMOUNT; i++)
        {
            std::cout << "[Rank#" << selfRank << "]: Data#" << i << " unpacked\n";
            std::cout << outputImgs[i];
            std::flush(std::cout);
        }
    #endif
    }

    std::cout << "[Rank#" << selfRank << "]: Total time: " << MPI_Wtime() - timeStart << std::endl;

    delete[] packedDataBuffer;
    delete[] outputImgs;
    MPI_Type_free(&grayScaleImgMpiType);
    MPI_Finalize();
	return 0;
}
