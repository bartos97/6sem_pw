#pragma once
#include <iostream>
#include <cstring>

class GrayScaleImage
{
public:
    GrayScaleImage() : GrayScaleImage("unnamed_file")
    {}

    GrayScaleImage(const char* name) : GrayScaleImage(name, 0, 0)
    {}

    GrayScaleImage(const char* name, unsigned width, unsigned height)
        : ID(counter++), width(width), height(height)
    {
        strcpy(fileName, name);
    }

    void createRandomData()
    {
        for (unsigned y = 0; y < height; y++)
        {
            for (unsigned x = 0; x < width; x++)
            {
                data[x + y * width] = rand() % 256;
            }
        }
    }

public:
    static const unsigned MAX_WIDTH = 64;
    static const unsigned MAX_HEIGHT = 64;
    static const unsigned MAX_FILENAME_LENGTH = 30;

    const unsigned ID;
    unsigned width;
    unsigned height;
    char fileName[MAX_FILENAME_LENGTH];
    unsigned char data[MAX_WIDTH * MAX_HEIGHT];

private:
    static unsigned counter;
};

std::ostream& operator<<(std::ostream& os, const GrayScaleImage& img);
