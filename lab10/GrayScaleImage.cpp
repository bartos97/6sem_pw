#include "GrayScaleImage.hpp"
#include <iomanip>

unsigned GrayScaleImage::counter;

std::ostream& operator<<(std::ostream& os, const GrayScaleImage& img)
{
    os << "Image #" << img.ID << " named: \"" << img.fileName << "\" here\n";
    
    for (unsigned y = 0; y < img.height; y++)
    {
        for (unsigned x = 0; x < img.width; x++)
        {
            os << std::setw(3) << +img.data[x + y * img.width] << ' ';
        }
        os << '\n';
    }
    return os;
}
