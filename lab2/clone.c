#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sched.h>

static int zmienna_globalna = 0;
static const size_t ROZMIAR_STOSU = 1 << 16;
static const size_t ITERATIONS = 5;

int funkcja_watku(void *argument)
{
    zmienna_globalna++;
    printf("#%d here, created by #%d; Global: %d\n", getpid(), getppid(), zmienna_globalna);

    int wynik;
    wynik = execv("./program.exe", NULL);
    if (wynik == -1)
        printf("Proces potomny nie wykonal programu\n");

    return 0;
}

int main()
{

    void *stos;
    pid_t pid;
    int i;

    stos = malloc(ROZMIAR_STOSU);
    if (stos == 0)
    {
        printf("Proces nadrzędny - blad alokacji stosu\n");
        exit(1);
    }

    for (i = 0; i < ITERATIONS; i++)
    {
        pid = clone(&funkcja_watku, (void *)stos + ROZMIAR_STOSU,
                    CLONE_FS | CLONE_FILES | CLONE_SIGHAND | CLONE_VM, 0);
        waitpid(pid, NULL, __WCLONE);
        printf("Proces #%d wlasnie spadl z rowerka\n", pid);
    }

    free(stos);
    return 0;
}
