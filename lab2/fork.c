#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "../pomiar_czasu/pomiar_czasu.h"

static const size_t ITERATIONS = 10;
static int zmienna_globalna = 0;

int main()
{
    int pid, wynik, i;

    // inicjuj_czas();
    for (i = 0; i < ITERATIONS; i++)
    {
        pid = fork();
        if (pid == 0)
        {
            zmienna_globalna++;

            wynik = execv("./program.exe", NULL);
            if (wynik == -1)
                printf("Proces potomny nie wykonal programu\n");

            exit(0);
        }
        else
        {
            wait(NULL);
        }
    }
    // drukuj_czas();

    return 0;
}
