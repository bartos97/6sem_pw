#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <unistd.h>
#include <errno.h>
#include "../pomiar_czasu/pomiar_czasu.h"

#define ARRAY_DIM_LENGTH 1000
#define ARRAY_SIZE ARRAY_DIM_LENGTH * ARRAY_DIM_LENGTH
#define DEFAULT_LOOPS 2
#define SCHEDULING_POLICY SCHED_RR

typedef int thread_arg_type;

static double a[ARRAY_SIZE], b[ARRAY_SIZE], c[ARRAY_SIZE];

static void checkError(int retCode, const char* msg)
{
    if (retCode)
    {
        printf("Error #%d: %s\n", retCode, msg);
        exit(EXIT_FAILURE);
    }
}

static void checkErrorErrno(int retCode, const char* msg)
{
    if (retCode == -1)
    {
        int error = errno;
        printf("Error #%d: %s\n", error, msg);
        exit(EXIT_FAILURE);
    }
}

static double busyWaiting()
{
    const size_t N = ARRAY_DIM_LENGTH;
    size_t i, j, k;

    for (i = 0; i < ARRAY_SIZE; i++)
        a[i] = (rand() / (double)RAND_MAX) * i;
        
    for (i = 0; i < ARRAY_SIZE; i++)
        b[i] = (rand() / (double)RAND_MAX) * (ARRAY_SIZE - i);

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            c[i + N * j] = 0.0;
            for (k = 0; k < N; k++)
            {
                c[i + N * j] += a[i + N * k] * b[k + N * j];
            }
        }
    }
    return (c[ARRAY_SIZE - 1]);
}

static void* threadFunction(void* threadArg)
{
    pthread_t id = pthread_self();
    thread_arg_type argCasted = threadArg == NULL 
        ? 1
        : *(thread_arg_type*)threadArg;

    struct sched_param schedParam;
    schedParam.sched_priority = argCasted;
    checkError(pthread_setschedparam(id, SCHEDULING_POLICY, &schedParam), "pthread_setschedparam");

    printf("\tThread #%ld starts, I'ma run %d loops of nonsense!\n", id, DEFAULT_LOOPS);
    inicjuj_czas();

    double res = 0.0;
    for (unsigned int i = 0; i < DEFAULT_LOOPS; i++)
        res = busyWaiting();

    printf("\tThread #%ld ends, it took me %lfs to compute this nonsense\n", id, czas_CPU());
    return NULL;
}

static pthread_t runThreadWithPriority(int* priority)
{
    pthread_t threadID;
    pthread_attr_t threadAttrs;
    
    checkError(pthread_attr_init(&threadAttrs), "pthread_attr_init");
    checkError(pthread_attr_setinheritsched(&threadAttrs, PTHREAD_EXPLICIT_SCHED), "pthread_attr_setinheritsched");

    checkError(pthread_create(&threadID, &threadAttrs, threadFunction, priority), "pthread_create");
    printf("Created thread #%ld with priority %d\n", threadID, *priority);

    return threadID;
}

static void waitForThread(pthread_t ID)
{
    checkError(pthread_join(ID, NULL), "thread join");
    printf("Joined  thread #%ld\n", ID);
}

int main()
{
    setbuf(stdout, NULL);

    int priorityMin = sched_get_priority_min(SCHEDULING_POLICY);
    int priorityMax = sched_get_priority_max(SCHEDULING_POLICY);
    checkErrorErrno(priorityMin, "priority min");
    checkErrorErrno(priorityMax, "priority max");

    pthread_t threadMin = runThreadWithPriority(&priorityMin);
    pthread_t threadMax = runThreadWithPriority(&priorityMax);
    
    waitForThread(threadMin);
    waitForThread(threadMax);
    
    exit(EXIT_SUCCESS);
}
