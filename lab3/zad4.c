#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <errno.h>
#include <string.h>

#define handle_error_en(en, msg) \
    do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define ARRAY_DIM_LENGTH 500
#define ARRAY_SIZE ARRAY_DIM_LENGTH * ARRAY_DIM_LENGTH
#define ONE_K (1<<10)
#define DEFAULT_LOOPS_AMOUNT 5
#define DEFAULT_THREADS_AMOUNT 10
#define MAX_THREADS_AMOUNT 10

typedef unsigned int thread_arg_type;

static double a[ARRAY_SIZE], b[ARRAY_SIZE], c[ARRAY_SIZE];
static size_t g_loopsAmount = DEFAULT_LOOPS_AMOUNT;
static size_t g_threadsAmount = DEFAULT_THREADS_AMOUNT;
static const size_t g_defaultStackSize = (1<<13) * ONE_K;
static int g_priorityMin;
static int g_priorityMax;

static void* threadFunction(void* threadArg);
static void generateRandomAttrs(pthread_attr_t* attrs);
static void checkError(int retCode, const char* msg);
static void checkErrorErrno(int retCode, const char* msg);
static double busyWaiting();
/**
 * from http://man7.org/linux/man-pages/man3/pthread_attr_init.3.html 
 * modified to print whole text at once
 */
static void display_pthread_attr();


int main(int argc, char const *argv[])
{
    setbuf(stdout, NULL);
    if (argc >= 2)
    {
        int tmp = atoi(argv[1]);
        if (tmp > 0 && tmp <= MAX_THREADS_AMOUNT)
            g_threadsAmount = (size_t)tmp;

        if (argc == 3)
        {
            tmp = atoi(argv[2]);
            if (tmp > 0)
                g_loopsAmount = (size_t)tmp;
        }
    }

    pthread_t threadIDs[MAX_THREADS_AMOUNT];
    pthread_attr_t threadAttrs[MAX_THREADS_AMOUNT];
    g_priorityMin = sched_get_priority_min(SCHED_RR);
    g_priorityMax = sched_get_priority_max(SCHED_RR);
    checkErrorErrno(g_priorityMin, "priority min");
    checkErrorErrno(g_priorityMax, "priority max");
    
    for (int i = 0; i < g_threadsAmount; i++)
    {
        pthread_attr_init(&threadAttrs[i]);
        generateRandomAttrs(&threadAttrs[i]);
        checkError(pthread_create(&threadIDs[i], &threadAttrs[i], threadFunction, NULL), "thread create");
        printf("Created #%ld\n", threadIDs[i]);
    }
    
    printf("Main thread goes away\n");
    pthread_exit(NULL);
}

static void* threadFunction(void* threadArg)
{
    pthread_t id = pthread_self();
    double res = 0.0;

    struct sched_param schedParam;
    schedParam.sched_priority = (rand() % (g_priorityMax - g_priorityMin)) + g_priorityMin;
    checkError(pthread_setschedparam(id, SCHED_RR, &schedParam), "pthread_setschedparam");

    printf("\tThread #%ld starts, I'ma run %d loops of nonsense!\n", id ,g_loopsAmount);
    display_pthread_attr();

    for (unsigned int i = 0; i < g_loopsAmount; i++)
        res = busyWaiting();

    printf("\tThread #%ld ends\n", id);
    return NULL;
}

static void generateRandomAttrs(pthread_attr_t* attrs)
{
    int retCode;

    retCode = pthread_attr_setinheritsched(attrs, PTHREAD_EXPLICIT_SCHED);
    checkError(retCode, "attr_setinheritsched");

    unsigned short stackGrow = ((rand() % 3) + 1);
    retCode = pthread_attr_setstacksize(attrs, g_defaultStackSize * stackGrow);
    checkError(retCode, "attr_setstacksize");

    if (rand() % 2)
    {
        retCode = pthread_attr_setdetachstate(attrs, PTHREAD_CREATE_DETACHED);
        checkError(retCode, "attr_setdetachstate");
    }
}

static void checkError(int retCode, const char* msg)
{
    if (retCode)
    {
        printf("%d ", retCode);
        handle_error_en(retCode, msg);
    }
}

static void checkErrorErrno(int retCode, const char* msg)
{
    if (retCode == -1)
    {
        printf("%d ", retCode);
        handle_error_en(retCode, msg);
    }
}


static double busyWaiting()
{
    const size_t N = ARRAY_DIM_LENGTH;
    size_t i, j, k;

    for (i = 0; i < ARRAY_SIZE; i++)
        a[i] = (rand() / (double)RAND_MAX) * i;
        
    for (i = 0; i < ARRAY_SIZE; i++)
        b[i] = (rand() / (double)RAND_MAX) * (ARRAY_SIZE - i);

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            c[i + N * j] = 0.0;
            for (k = 0; k < N; k++)
            {
                c[i + N * j] += a[i + N * k] * b[k + N * j];
            }
        }
    }
    return (c[ARRAY_SIZE - 1]);
}

static void display_pthread_attr()
{
    int s, i;
    size_t v;
    void *stkaddr;
    struct sched_param sp;

    pthread_attr_t actualAttr;
    pthread_attr_t* attr = &actualAttr;
    checkError(pthread_getattr_np(pthread_self(), attr), "Get self attributes");

    char prefix[128];
    sprintf(prefix, "\t\t#%ld: ", pthread_self());
    char buffer[256];
    char finalPrint[512 + 128] = "";

    s = pthread_attr_getdetachstate(attr, &i);
    if (s != 0)
        handle_error_en(s, "pthread_attr_getdetachstate");
    sprintf(buffer, "%sDetach state        = %s\n", prefix,
           (i == PTHREAD_CREATE_DETACHED) ? "PTHREAD_CREATE_DETACHED" : (i == PTHREAD_CREATE_JOINABLE) ? "PTHREAD_CREATE_JOINABLE" : "???");
    strcat(finalPrint, buffer);

    // s = pthread_attr_getscope(attr, &i);
    // if (s != 0)
    //     handle_error_en(s, "pthread_attr_getscope");
    // sprintf(buffer, "%sScope               = %s\n", prefix,
    //        (i == PTHREAD_SCOPE_SYSTEM) ? "PTHREAD_SCOPE_SYSTEM" : (i == PTHREAD_SCOPE_PROCESS) ? "PTHREAD_SCOPE_PROCESS" : "???");
    // strcat(finalPrint, buffer);


    s = pthread_attr_getinheritsched(attr, &i);
    if (s != 0)
        handle_error_en(s, "pthread_attr_getinheritsched");
    sprintf(buffer, "%sInherit scheduler   = %s\n", prefix,
           (i == PTHREAD_INHERIT_SCHED) ? "PTHREAD_INHERIT_SCHED" : (i == PTHREAD_EXPLICIT_SCHED) ? "PTHREAD_EXPLICIT_SCHED" : "???");
    strcat(finalPrint, buffer);

    s = pthread_attr_getschedpolicy(attr, &i);
    if (s != 0)
        handle_error_en(s, "pthread_attr_getschedpolicy");
    sprintf(buffer, "%sScheduling policy   = %s\n", prefix,
           (i == SCHED_OTHER) ? "SCHED_OTHER" : (i == SCHED_RR) ? "SCHED_RR" : (i == SCHED_RR) ? "SCHED_RR" : "???");
    strcat(finalPrint, buffer);

    s = pthread_attr_getschedparam(attr, &sp);
    if (s != 0)
        handle_error_en(s, "pthread_attr_getschedparam");
    sprintf(buffer, "%sScheduling priority = %d\n", prefix, sp.sched_priority);
    strcat(finalPrint, buffer);

    // s = pthread_attr_getguardsize(attr, &v);
    // if (s != 0)
    //     handle_error_en(s, "pthread_attr_getguardsize");
    // sprintf(buffer, "%sGuard size          = %d bytes\n", prefix, v);
    // strcat(finalPrint, buffer);

    s = pthread_attr_getstack(attr, &stkaddr, &v);
    if (s != 0)
        handle_error_en(s, "pthread_attr_getstack");
    // sprintf(buffer, "%sStack address       = %p\n", prefix, stkaddr);
    // strcat(finalPrint, buffer);

    sprintf(buffer, "%sStack size          = %ld KB\n", prefix, v / (1<<10));
    strcat(finalPrint, buffer);

    printf("%s", finalPrint);
}
