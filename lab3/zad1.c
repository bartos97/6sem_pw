#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

typedef const char* thread_arg_type;

static void* threadFunction(void* threadArg)
{
    thread_arg_type* argCasted = (thread_arg_type*)threadArg;
    printf("\tWazzap! New thread #%ld here. You passed me: %s\n", pthread_self(), *argCasted);
    return NULL;
}

int main(int argc, char const *argv[])
{
    const size_t THREADS_AMOUNT = argc == 2 ? atoi(argv[1]) : 10;
    pthread_t threadIDs[THREADS_AMOUNT];
    thread_arg_type arg = "secret message!";

    for (size_t i = 0; i < THREADS_AMOUNT; i++)
    {
        int threadCreateRetCode = pthread_create(&threadIDs[i], NULL, threadFunction, (void*)&arg);
        if (threadCreateRetCode)
        {
            printf("Couldn't create new thread, return code: %d\n", threadCreateRetCode);
            exit(EXIT_FAILURE);
        }
        else
        {
            printf("Created thread #%ld\n", threadIDs[i]);
        }
        
    }
    
    for (size_t i = 0; i < THREADS_AMOUNT; i++)
    {
        int threadJoinRetCode;
        if (pthread_join(threadIDs[i], (void*)&threadJoinRetCode))
        {
            printf("Thread #%dl join error: %d", threadIDs[i], threadJoinRetCode);
        }
        else
        {
            printf("Thread #%ld joined successfully with status %d\n", threadIDs[i], threadJoinRetCode);
        }
        
    }
    
    exit(EXIT_SUCCESS);
}
