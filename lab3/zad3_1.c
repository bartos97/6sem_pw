#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define ARRAY_DIM_LENGTH 1000
#define ARRAY_SIZE ARRAY_DIM_LENGTH * ARRAY_DIM_LENGTH
#define THREADS_AMOUNT 4
#define MAX_LOOPS 20

typedef unsigned int thread_arg_type;

static double a[ARRAY_SIZE], b[ARRAY_SIZE], c[ARRAY_SIZE];

static double busyWaiting()
{
    const size_t N = ARRAY_DIM_LENGTH;
    size_t i, j, k;

    for (i = 0; i < ARRAY_SIZE; i++)
        a[i] = (rand() / (double)RAND_MAX) * i;
        
    for (i = 0; i < ARRAY_SIZE; i++)
        b[i] = (rand() / (double)RAND_MAX) * (ARRAY_SIZE - i);

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            c[i + N * j] = 0.0;
            for (k = 0; k < N; k++)
            {
                c[i + N * j] += a[i + N * k] * b[k + N * j];
            }
        }
    }
    return (c[ARRAY_SIZE - 1]);
}

static void* threadFunction(void* threadArg)
{
    pthread_t id = pthread_self();
    thread_arg_type argCasted = *(thread_arg_type*)threadArg;
    
    printf("\tThread #%ld starts, I'ma run %d loops of nonsense!\n", id ,argCasted);

    double res = 0.0;
    for (unsigned int i = 0; i < argCasted; i++)
    {
        res = busyWaiting();
    }

    printf("\tThread #%ld ends, the last nonsense I've computed is %lf\n", id, res);
    return NULL;
}

int main()
{
    pthread_t threadIDs[THREADS_AMOUNT];
    unsigned int busyLoops;

    for (size_t i = 0; i < THREADS_AMOUNT; i++)
    {
        busyLoops = rand() % MAX_LOOPS + 1;
        if (pthread_create(&threadIDs[i], NULL, threadFunction, &busyLoops))
        {
            printf("Error\n");
            exit(EXIT_FAILURE);
        }
        else
        {
            printf("Created thread #%ld\n", threadIDs[i]);
        }
    }
    
    pthread_exit(NULL);
}
