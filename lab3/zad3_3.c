#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>

#define ARRAY_DIM_LENGTH 1000
#define ARRAY_SIZE ARRAY_DIM_LENGTH * ARRAY_DIM_LENGTH

typedef unsigned int thread_arg_type;

static double a[ARRAY_SIZE], b[ARRAY_SIZE], c[ARRAY_SIZE];

static double busyWaiting()
{
    const size_t N = ARRAY_DIM_LENGTH;
    size_t i, j, k;

    for (i = 0; i < ARRAY_SIZE; i++)
        a[i] = (rand() / (double)RAND_MAX) * i;
        
    for (i = 0; i < ARRAY_SIZE; i++)
        b[i] = (rand() / (double)RAND_MAX) * (ARRAY_SIZE - i);

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            c[i + N * j] = 0.0;
            for (k = 0; k < N; k++)
            {
                c[i + N * j] += a[i + N * k] * b[k + N * j];
            }
        }
    }
    return (c[ARRAY_SIZE - 1]);
}

static void* threadFunction(void* threadArg)
{
    pthread_t id = pthread_self();
    thread_arg_type argCasted = threadArg == NULL ? 1 : *(thread_arg_type*)threadArg;

    printf("\tThread #%ld starts, I'ma run %d loops of nonsense!\n", id ,argCasted);

    double res = 0.0;
    for (unsigned int i = 0; i < argCasted; i++)
        res = busyWaiting();

    printf("\tThread #%ld ends, the last nonsense I've computed is %lf\n", id, res);
    return NULL;
}

int main()
{
    pthread_t threadID;
    pthread_attr_t threadAttrs;
    cpu_set_t cpuset;

    CPU_ZERO(&cpuset);
    CPU_SET(2, &cpuset);
    pthread_attr_init(&threadAttrs);
    pthread_attr_setaffinity_np(&threadAttrs, sizeof(cpu_set_t), &cpuset);

    unsigned int loopsAmount = 10;
    if (pthread_create(&threadID, &threadAttrs, threadFunction, &loopsAmount))
    {
        printf("Fuckup when creating thread\n");
        exit(EXIT_FAILURE);
    }
    else printf("Created thread #%ld\n", threadID);

    if (pthread_join(threadID, NULL))
    {
        printf("Fuckup when joining\n");
        exit(EXIT_FAILURE);
    }
    else printf("Joined thread #%ld\n", threadID);
    
    exit(EXIT_SUCCESS);
}
