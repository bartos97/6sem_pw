#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <stdatomic.h>

#define WYMIAR 1000
#define ROZMIAR WYMIAR * WYMIAR

#define TASK1
#define TASK2
#define TASK3

static int zmienna_wspolna = 0;
static double a[ROZMIAR], b[ROZMIAR], c[ROZMIAR];
static volatile atomic_bool flag_thread_completed;


static void cleanup_handler(void *arg)
{
    atomic_store(&flag_thread_completed, 1);
}

double czasozajmowacz()
{
    int i, j, k;
    int n = WYMIAR;
    for (i = 0; i < ROZMIAR; i++)
        a[i] = 1.0 * i;
    for (i = 0; i < ROZMIAR; i++)
        b[i] = 1.0 * (ROZMIAR - i);
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            c[i + n * j] = 0.0;
            for (k = 0; k < n; k++)
            {
                c[i + n * j] += a[i + n * k] * b[k + n * j];
            }
        }
    }
    return (c[ROZMIAR - 1]);
}

void *zadanie_watku(void *arg_wsk)
{
    pthread_cleanup_push(cleanup_handler, NULL);

    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
    printf("\twatek potomny: uniemozliwione zabicie\n");

    czasozajmowacz();

    printf("\twatek potomny: umozliwienie zabicia\n");
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

    pthread_testcancel();

    zmienna_wspolna++;
    printf("\twatek potomny: zmiana wartosci zmiennej wspolnej\n");

    pthread_cleanup_pop(1); // zeby handler sie zawsze odpalił, nie tylko na cancel z zewnatrz itp
    return (NULL);
}

int main()
{
    pthread_t tid;
    pthread_attr_t attr;
    void *wynik;
    int i;

    /********************
     * Wątek przyłączalny
     ********************/
#ifdef TASK1
    printf("watek glowny: tworzenie watku potomnego nr 1\n");
    pthread_create(&tid, NULL, zadanie_watku, NULL); //jeśli pthread_attr* == NULL to stworzy domyślne z automatu
    sleep(2); // czas na uruchomienie watku

    printf("\twatek glowny: wyslanie sygnalu zabicia watku\n");
    pthread_cancel(tid);

    pthread_join(tid, &wynik); //Co nalezy zrobić przed sprawdzeniem czy wątki się skonczyły?
    if (wynik == PTHREAD_CANCELED)
        printf("\twatek glowny: watek potomny 1 zostal zabity\n");
    else
        printf("\twatek glowny: watek potomny 1 NIE zostal zabity - blad\n");
#endif

    /********************
     * Wątek odłączony
     ********************/
#ifdef TASK2
    zmienna_wspolna = 0;
    atomic_store(&flag_thread_completed, 0);

    printf("watek glowny: tworzenie watku potomnego nr 2\n");
    pthread_create(&tid, NULL, zadanie_watku, NULL);
    sleep(2); // czas na uruchomienie watku
    printf("\twatek glowny: odlaczenie watku potomnego\n");
    pthread_detach(tid); //Instrukcja odłączenia?
    printf("\twatek glowny: wyslanie sygnalu zabicia watku odlaczonego\n");
    pthread_cancel(tid);

    // Czy watek został zabity? Jak to sprawdzić? 
    // BZ: Za pomocą cleanup handlera, atomowej flagi i aktywnego czekania (not great but works)
    while (atomic_load(&flag_thread_completed) == 0);
    printf("\twatek glowny: watek potomny 2 zostal zabity\n");
#endif

#ifdef TASK3
    //Inicjacja atrybutów?
    // BZ: Prosz...
    pthread_attr_init(&attr);

    // Ustawianie typu watku na odłaczony
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    printf("watek glowny: tworzenie odlaczonego watku potomnego nr 3\n");
    pthread_create(&tid, &attr, zadanie_watku, NULL);

    //Niszczenie atrybutów
    pthread_attr_destroy(&attr);

    printf("\twatek glowny: koniec pracy, watek odlaczony pracuje dalej\n");
    pthread_exit(NULL); 
    // co stanie sie gdy uzyjemy exit(0)?
    // BZ: wątek potomny zostanie również ubity. 
    // A tak to tylko wątek główny zostanie zakończony, potomny poleci do końca i dopiero potem cały proces się zakończy
#endif
}
