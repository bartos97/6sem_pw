#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define ARRAY_DIM_LENGTH 100
#define ARRAY_SIZE ARRAY_DIM_LENGTH * ARRAY_DIM_LENGTH
#define ONE_K (1<<10)
#define STACK_SIZE ((1<<14) * ONE_K)

typedef unsigned int thread_arg_type;

static double a[ARRAY_SIZE], b[ARRAY_SIZE], c[ARRAY_SIZE];

static double busyWaiting()
{
    const size_t N = ARRAY_DIM_LENGTH;
    size_t i, j, k;

    for (i = 0; i < ARRAY_SIZE; i++)
        a[i] = (rand() / (double)RAND_MAX) * i;
        
    for (i = 0; i < ARRAY_SIZE; i++)
        b[i] = (rand() / (double)RAND_MAX) * (ARRAY_SIZE - i);

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            c[i + N * j] = 0.0;
            for (k = 0; k < N; k++)
            {
                c[i + N * j] += a[i + N * k] * b[k + N * j];
            }
        }
    }
    return (c[ARRAY_SIZE - 1]);
}

static void* threadFunction(void* threadArg)
{
    pthread_t id = pthread_self();
    thread_arg_type argCasted = *(thread_arg_type*)threadArg;

    printf("\tThread #%ld starts, I'ma run %d loops of nonsense!\n", id ,argCasted);

    // 2 MB: spowoduje seg fault (u mnie przynajmniej)
    // double letsStackOverflow[STACK_SIZE / (1<<3)] = {0};

    // 1.875 MB: powinno już przejsc
    #define THREAD_STACK_ALLOC (STACK_SIZE / (1<<4) + STACK_SIZE / (1<<5) + STACK_SIZE / (1<<6) + STACK_SIZE / (1<<7) )

    printf("\tThread #%ld here and you won't stop me from allocating %ld KB on stack\n", id, THREAD_STACK_ALLOC / ONE_K);
    double letsStackOverflow[THREAD_STACK_ALLOC] = {0};

    double res = 0.0;
    for (unsigned int i = 0; i < argCasted; i++)
        res = busyWaiting();

    printf("\tThread #%ld ends, the last nonsense I've computed is %lf\n", id, res);
    return NULL;
}

int main()
{
    pthread_t threadID;
    pthread_attr_t threadAttrs;
    size_t stacksize;

    pthread_attr_init(&threadAttrs);

    pthread_attr_getstacksize(&threadAttrs, &stacksize);
    printf("Default stack size: %ld KB\n", stacksize / ONE_K);
    
    pthread_attr_setstacksize(&threadAttrs, STACK_SIZE);

    unsigned int loops = 1;
    if (pthread_create(&threadID, &threadAttrs, threadFunction, &loops))
    {
        printf("Error\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        pthread_attr_getstacksize(&threadAttrs, &stacksize);
        printf("Created thread #%ld with %ld KB stack\n", threadID, stacksize / ONE_K);
    }

    if (pthread_join(threadID, NULL))
    {
        printf("Error when joining\n");
        exit(EXIT_FAILURE);
    }
    else printf("Joined thread #%ld\n", threadID);
    
    exit(EXIT_SUCCESS);
}
