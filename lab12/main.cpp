#include "PBMImage.hpp"
#include "PredefinedKernels.hpp"
#include <mpi.h>


std::pair<size_t, size_t> getRangeForProc(int procRank, size_t allItemsAmount, int numOfProcesses)
{
    const size_t perRankChunkCount = allItemsAmount / numOfProcesses;
    const size_t leftoverChunkCount = allItemsAmount % numOfProcesses;
    size_t startTranslation = 0;
    size_t endTranslation = 0;

    if (leftoverChunkCount > 0)
    {
        bool anythingToDispense = long(leftoverChunkCount - procRank) > 0;
        startTranslation = anythingToDispense ? procRank : leftoverChunkCount;
        endTranslation = anythingToDispense ? 1 : 0;
    }

    const size_t startIndex = procRank * perRankChunkCount + startTranslation;
    const size_t pastEndIndex = startIndex + perRankChunkCount + endTranslation;

    return std::make_pair(startIndex, pastEndIndex);
}


int main(int argc, char* argv[])
{
    std::locale::global(std::locale(""));
    std::cout.imbue(std::locale());

    int numOfProcesses, selfRank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numOfProcesses);
    MPI_Comm_rank(MPI_COMM_WORLD, &selfRank);

    if (numOfProcesses == 1)
    {
        std::cerr << "Must be at least 2 processes; use main_sync instead" << std::endl;
        MPI_Finalize();
        return 0;
    }

    const std::string fileName = argc > 1 ? argv[1] : "casablanca.pgm";
    const std::string outFileName = "out_" + fileName;
    constexpr auto kernel = PredefinedKernels::sobelFilter0deg;
    constexpr int rootID = 0;

    // all ranks have their copy of original image
    PBMImage imgIn{fileName.c_str()};
    const double timeStart = MPI_Wtime();

    // all non-root ranks are doing kernel convolution in seperate ranges
    if (selfRank != rootID)
    {
        auto range = getRangeForProc(selfRank - 1, imgIn.height, numOfProcesses - 1);
    #ifndef RELEASE_MODE
        std::cout << "Proc #" << selfRank << " takes rows from " << range.first << " to " << range.second << std::endl;
    #endif

        PBMImage imgOut{imgIn.width, range.second - range.first}; //does malloc
        imgIn.applyKernelOnRows(kernel, PredefinedKernels::size, range.first, range.second, imgOut);

        MPI_Send(imgOut.data, imgOut.width * imgOut.height, MPI_BYTE, rootID, 0, MPI_COMM_WORLD);
    }

    // root rank receives data from others with kernel applied
    // and saves it to it's imgIn object
    if (selfRank == rootID)
    {
        MPI_Status status;
        for (unsigned i = 1; i < numOfProcesses; i++)
        {
            MPI_Probe(MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
            auto range = getRangeForProc(status.MPI_SOURCE - 1, imgIn.height, numOfProcesses - 1);
            MPI_Recv(&(imgIn.data[imgIn.width * range.first]), imgIn.width * (range.second - range.first), MPI_BYTE,
                     status.MPI_SOURCE, 0, MPI_COMM_WORLD, &status);
        }

        std::cout << "Filter application time: " << MPI_Wtime() - timeStart << "s" << std::endl;
        imgIn.saveToFile(outFileName.c_str());
    }

    MPI_Finalize();
    return 0;
}
