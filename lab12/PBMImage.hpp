#pragma once
#include <cstddef>

class PBMImage
{
public:
    using Byte = unsigned char;
    static constexpr unsigned MessageLength = 20;
    static constexpr unsigned HeaderLength = 10;
    static constexpr unsigned TypeLength = 2;

    PBMImage() = default;
    PBMImage(const PBMImage&);
    PBMImage(const char* fileName);
    PBMImage(size_t width, size_t height);
    ~PBMImage();

    PBMImage deepCopy() const;

    void saveToFile(const char* fileName) const;
    void applyKernel(const float kernel[], const unsigned kernelSize, PBMImage& outImg) const;
    void applyKernelOnRows(const float kernel[], const unsigned kernelSize, size_t rowStartIndex,
                           size_t rowPastEndIndex, PBMImage& outImg) const;

private:
    inline void applyKernelOnPixelTranslatedRow(size_t xPixel, size_t yPixel, const float kernel[],
                                                const unsigned kernelSize, PBMImage& outImg,
                                                const size_t rowTranslation) const;
    inline void applyKernelOnPixel(size_t xPixel, size_t yPixel, const float kernel[], const unsigned kernelSize,
                                   PBMImage& outImg) const;

public:
    size_t width = 0;
    size_t height = 0;
    int maxValue = 0;
    Byte* data = nullptr;
    Byte type[3];

private:
    bool isOpened = false;
};