namespace PredefinedKernels
{

constexpr unsigned size = 3;

constexpr float gaussianBlur[] = {
    1.0f / 16.0f, 2.0f / 16.0f, 1.0f / 16.0f,
    2.0f / 16.0f, 4.0f / 16.0f, 2.0f / 16.0f,
    1.0f / 16.0f, 2.0f / 16.0f, 1.0f / 16.0f
};

constexpr float meanBlur[] = {
    0.1f, 0.1f, 0.1f,
    0.1f, 0.1f, 0.1f,
    0.1f, 0.1f, 0.1f
};

constexpr float sobelFilter0deg[] = {
    1.0f, 0.0f, -1.0f,
    2.0f, 0.0f, -2.0f,
    1.0f, 0.0f, -1.0f
};

constexpr float sobelFilter45deg[] = {
     0.0f,  1.0f, 2.0f,
    -1.0f,  0.0f, 1.0f,
    -2.0f, -1.0f, 0.0f
};

constexpr float sobelFilter90deg[] = {
     1.0f,  2.0f,  1.0f,
     0.0f,  0.0f,  0.0f
    -1.0f, -2.0f, -1.0f
};

}