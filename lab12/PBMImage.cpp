#include "PBMImage.hpp"
#include <cstdio>
#include <cstdlib>
#include <cstring>

PBMImage::PBMImage(const char* fileName)
{
    /* we first open the file */
    FILE* input_file = fopen(fileName, "rb");
    if (input_file == 0)
    {
        perror("The file couldn't be opened");
        return;
    }

    /* reads the header of the file to see the format */
    char file_header[HeaderLength];
    fgets(file_header, HeaderLength, input_file);

    /* sets the type of the image */
    memcpy(type, file_header, TypeLength);
    type[2] = '\0';
    if (memcmp(type, "P5", TypeLength) != 0)
    {
        puts("The header type is wrong");
        fclose(input_file);
        return;
    }

    /* reads with and height of the image */
    if (fscanf(input_file, "%ld %ld\n", &(width), &(height)) != 2)
    {
        puts("The height and the width were not read correctly");
        fclose(input_file);
        return;
    }

    /* reads maxValue */
    if (fscanf(input_file, "%d\n", &(maxValue)) != 1)
    {
        puts("The max value was not read correctly");
        fclose(input_file);
        return;
    }

    /* reading the image content */
    if (memcmp(type, "P5", TypeLength) == 0)
    {
        data = (Byte*)malloc(sizeof(Byte) * height * width);
        if (data != nullptr)
        {
            fread(data, width, height, input_file);
            isOpened = true;
        }
    }

    fclose(input_file);
#ifndef RELEASE_MODE
    printf("Opened file %s %dx%d\n", fileName, width, height);
#endif
}

PBMImage::~PBMImage()
{
    if (isOpened)
        free(data);
}

PBMImage::PBMImage(const PBMImage& other)
{
    if (!other.isOpened)
        return;

    width = other.width;
    height = other.height;
    maxValue = other.maxValue;
    isOpened = true;
    memcpy(type, other.type, TypeLength);
    type[2] = '\0';

    size_t size = sizeof(Byte) * height * width;
    data = (Byte*)malloc(size);
    if (data == nullptr)
    {
        perror("Malloc failed");
        return;
    }
}

PBMImage::PBMImage(size_t width, size_t height)
    : width(width)
    , height(height)
{
    size_t size = sizeof(Byte) * height * width;
    data = (Byte*)malloc(size);
    if (data == nullptr)
    {
        perror("Malloc failed");
        return;
    }
    isOpened = true;
}

PBMImage PBMImage::deepCopy() const
{
    if (!isOpened)
        return PBMImage{};

    PBMImage newObj = *this;
    memcpy(newObj.data, data, sizeof(Byte) * height * width);

    printf("Copied img %dx%d\n", width, height);
    return newObj;
}

void PBMImage::saveToFile(const char* fileName) const
{
    FILE* output_file = fopen(fileName, "wb+");
    if (output_file == 0)
    {
        puts("The file couldn't be opened");
    }

    /* wrinting the type of the image */
    fprintf(output_file, "%s\n", type);

    /* writing the width and the height of the image */
    fprintf(output_file, "%d %d\n", width, height);

    /* writng the maxValue*/
    fprintf(output_file, "%d\n", maxValue);

    /* write data back to file */
    // fwrite(data, width, height, output_file);
    fwrite(data, sizeof(Byte), width * height, output_file);

    fclose(output_file);
}

void PBMImage::applyKernel(const float kernel[], const unsigned kernelSize, PBMImage& outImg) const
{
    if (width != outImg.width || height != outImg.height)
    {
        printf("Images must have the same resolution\n");
        return;
    }

    for (size_t y = 0; y < height; y++)
    {
        for (size_t x = 0; x < width; x++)
        {
            applyKernelOnPixel(x, y, kernel, kernelSize, outImg);
        }
    }
}

void PBMImage::applyKernelOnRows(const float kernel[], const unsigned kernelSize, size_t rowStartIndex,
                                 size_t rowPastEndIndex, PBMImage& outImg) const
{
    if (width != outImg.width)
    {
        printf("Images must have the same width\n");
        return;
    }

    for (size_t y = rowStartIndex; y < rowPastEndIndex; y++)
    {
        for (size_t x = 0; x < width; x++)
        {
            applyKernelOnPixelTranslatedRow(x, y, kernel, kernelSize, outImg, rowStartIndex);
        }
    }
}

inline void PBMImage::applyKernelOnPixel(size_t xPixel, size_t yPixel, const float kernel[], const unsigned kernelSize,
                                         PBMImage& outImg) const
{
    applyKernelOnPixelTranslatedRow(xPixel, yPixel, kernel, kernelSize, outImg, 0);
}

inline void PBMImage::applyKernelOnPixelTranslatedRow(size_t xPixel, size_t yPixel, const float kernel[],
                                                      const unsigned kernelSize, PBMImage& outImg,
                                                      const size_t rowTranslation) const
{
    const unsigned kernelHalfSize = kernelSize / 2;
    size_t x, y;
    Byte sum = 0;

    for (size_t yKernel = 0; yKernel < kernelSize; yKernel++)
    {
        if (yPixel + yKernel < kernelHalfSize)
            y = 0;
        else if (yPixel + yKernel > height - 1)
            y = height - 1;
        else
            y = yPixel + yKernel - kernelHalfSize;

        for (size_t xKernel = 0; xKernel < kernelSize; xKernel++)
        {
            if (xPixel + xKernel < kernelHalfSize)
                x = 0;
            else if (xPixel + xKernel > width - 1)
                x = width - 1;
            else
                x = xPixel + xKernel - kernelHalfSize;

            sum += data[x + y * width] * kernel[xKernel + yKernel * kernelSize];
        }
    }

    outImg.data[xPixel + (yPixel - rowTranslation) * outImg.width] = sum;
}
