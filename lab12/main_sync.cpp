#include "PBMImage.hpp"
#include "PredefinedKernels.hpp"
#include <mpi.h>

#define FULL ;

int main(int argc, char* argv[])
{
    std::locale::global(std::locale(""));
    std::cout.imbue(std::locale());

    const std::string fileName = argc > 1 ? argv[1] : "casablanca.pgm";
    const std::string outFileName = "out_" + fileName;
    constexpr auto kernel = PredefinedKernels::sobelFilter0deg;

    double timeStart, timeEnd;
    const PBMImage imgIn{fileName.c_str()};

    timeStart = MPI_Wtime();

#ifdef FULL
    PBMImage imgOut = imgIn; //does malloc
    imgIn.applyKernel(kernel, PredefinedKernels::size, imgOut);
#else
    // for test
    PBMImage imgOut = imgIn;
    imgOut.height = imgIn.height / 2;
    imgIn.applyKernelOnRows(kernel, PredefinedKernels::size, imgIn.height / 2, imgIn.height,
                            imgOut);
#endif

    timeEnd = MPI_Wtime();
    std::cout << "Filter application time: " << timeEnd - timeStart << "s" << std::endl;
    imgOut.saveToFile(outFileName.c_str());

    return 0;
}
