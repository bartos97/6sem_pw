#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <string.h>
#include "../utils/array_utils.h"
#include "../utils/pthread_utils.h"


static void matrixMultiply(int* array1, int* array2, int* arrayResult, size_t array1_rowCout, size_t array1_colCount, size_t array2_colCount);

/**
 * array1_rowCount, array1_colCount, array2_colCount
 */
int main(int argc, char const *argv[])
{
#ifndef NO_FILE
    char fileName[64] = "result";

    #ifndef NO_PARALLEL
        #ifdef SCHED_STATIC
            strcat(fileName, "_static");
        #endif
        #ifdef SCHED_DYNAMIC
            strcat(fileName, "_dynamic");
        #endif
        #ifdef PLACE_IN
            strcat(fileName, "_in");
        #endif
        #ifdef PLACE_OUT
            strcat(fileName, "_out");
        #endif
        #ifdef PLACE_SUM
            strcat(fileName, "_sum");
        #endif
    #else
        strcat(fileName, "_sync");
    #endif

    strcat(fileName, ".dat");
    FILE* file = fopen(fileName, "w");
    CUSTOM_ASSERT(file, "Error when opening / creating file\n");
#endif

    double timeStart, timeEnd, timeTotal;

    const size_t array1_rowCount = argc > 1 ? atoi(argv[1]) : 5;
    const size_t array1_colCount = argc > 2 ? atoi(argv[2]) : array1_rowCount;
    const size_t array1_count = array1_rowCount * array1_colCount;

    const size_t array2_rowCount = array1_colCount;
    const size_t array2_colCount = argc > 3 ? atoi(argv[3]) : array1_rowCount;
    const size_t array2_count = array2_rowCount * array2_colCount;
    
    const size_t arrayResult_rowCount = array1_rowCount;
    const size_t arrayResult_colCount = array2_colCount;
    const size_t arrayResult_count = arrayResult_rowCount * arrayResult_colCount;

    int* const array1 = (int*)malloc(array1_count * sizeof(int));
    CUSTOM_ASSERT(array1, "malloc failed");
    int* const array2 = (int*)malloc(array2_count * sizeof(int));
    CUSTOM_ASSERT(array2, "malloc failed");
    int* const arrayResult = (int*)calloc(arrayResult_count, sizeof(int));
    CUSTOM_ASSERT(arrayResult, "malloc failed");

    fillArrayWithRandom(array1, array1_count, 10, 0);
    fillArrayWithRandom(array2, array2_count, 10, 0);

#ifndef RELEASE_MODE
    printf("matrix1: \n");
    printFlattenArray(array1, array1_count, array1_rowCount, array1_colCount);
    printf("matrix2: \n");
    printFlattenArray(array2, array2_count, array2_rowCount, array2_colCount);
#endif
#ifndef NO_FILE
    fprintf(file, "wolfram notation: \n");
    printFlattenArrayToFileWolframNotation(array1, array1_count, array1_rowCount, array1_colCount, file);
    fprintf(file, " . ");
    printFlattenArrayToFileWolframNotation(array2, array2_count, array2_rowCount, array2_colCount, file);
    fprintf(file, "\n\n");
    printFlattenArrayToFile(array1, array1_count, array1_rowCount, array1_colCount, file);
    fprintf(file, ".\n");
    printFlattenArrayToFile(array2, array2_count, array2_rowCount, array2_colCount, file);
#endif

    timeStart = omp_get_wtime();

    matrixMultiply(array1, array2, arrayResult, array1_rowCount, array1_colCount, array2_colCount);

    timeEnd = omp_get_wtime();
    timeTotal = timeEnd - timeStart;

#ifndef RELEASE_MODE
    printf("matrixResult: \n");
    printFlattenArray(arrayResult, arrayResult_count, arrayResult_rowCount, arrayResult_colCount);
#endif
    printf("Total time: %.17gs\n", timeTotal);
#ifndef NO_FILE
    fprintf(file, "=\n");
    printFlattenArrayToFile(arrayResult, arrayResult_count, arrayResult_rowCount, arrayResult_colCount, file);
    fprintf(file, "Total time: %.17gs\n", timeTotal);
    fclose(file);
#endif

    exit(EXIT_SUCCESS);
}


static void matrixMultiply(int* array1, int* array2, int* arrayResult, size_t array1_rowCout, size_t array1_colCount, size_t array2_colCount)
{
#ifdef NO_PARALLEL

    for (size_t row = 0; row < array1_rowCout; row++)
    {
        for (size_t col = 0; col < array2_colCount; col++)
        {
            for (size_t i = 0; i < array1_colCount; i++)
            {
                //result[row, col] += array1[row][i] * array2[i][col]
                arrayResult[row *array2_colCount+ col] += array1[row *array1_colCount+ i] * array2[i *array2_colCount+ col];
            }
        }
    }

#else

    long sum;
    #ifdef PLACE_OUT
        #ifdef SCHED_STATIC
            #pragma omp parallel for schedule(static)
        #endif
        #ifdef SCHED_DYNAMIC
            #pragma omp parallel for schedule(dynamic, 10)
        #endif
    #endif
    for (size_t row = 0; row < array1_rowCout; row++)
    {
        #ifdef PLACE_IN
            #ifdef SCHED_STATIC
                #pragma omp parallel for schedule(static)
            #endif
            #ifdef SCHED_DYNAMIC
                #pragma omp parallel for schedule(dynamic, 10)
            #endif
        #endif
        for (size_t col = 0; col < array2_colCount; col++)
        {
            sum = 0;
            #ifdef PLACE_SUM
                #ifdef SCHED_STATIC
                    #pragma omp parallel for reduction(+:sum) schedule(static)
                #endif
                #ifdef SCHED_DYNAMIC
                    #pragma omp parallel for reduction(+:sum) schedule(dynamic, 10)
                #endif
            #endif
            for (size_t i = 0; i < array1_colCount; i++)
            {
                sum += array1[row *array1_colCount+ i] * array2[i *array2_colCount+ col];
            }
            arrayResult[row *array2_colCount+ col] = sum;
        }
    }

#endif
}