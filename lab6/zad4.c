#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <omp.h>

int main(int argc, char const *argv[])
{
    srand(time(0));
    const int number = argc > 1 ? atoi(argv[1]) : rand() % 100;
    unsigned long long sum = 0;

    #pragma omp parallel for schedule(static)
    for (size_t i = 0; i < 500; i++)
    {
        #pragma omp critical
        {
            sum += number * number;
        }
    }
    
    printf("Liczba: %d suma: %d\n", number, sum);
    exit(EXIT_SUCCESS);
}
