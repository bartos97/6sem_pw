#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <unistd.h>

#define NUM_LOOPS 15
#define SLEEP_EVERY_N 3

#define DO_PRAGMA(x) _Pragma (#x)

#define TEST_OMP(param)\
{\
    int a[NUM_LOOPS];\
    double timeStart = omp_get_wtime();\
    printf("TEST: omp parallel for shared(a) "#param"\n");\
    \
    DO_PRAGMA(omp parallel for shared(a) param)\
    for (size_t i = 0; i < NUM_LOOPS; i++)\
    {\
        if ((i % SLEEP_EVERY_N) == 0)\
            sleep(0);\
        a[i] = omp_get_thread_num();\
    }\
    printSummary(a, NUM_LOOPS, omp_get_wtime() - timeStart);\
}

void printSummary(int* arr, const size_t arrNum, double time)
{
    printf("Time: %lfs; ", time);
    printf("threads sequence: [");

    for (size_t i = 0; i < arrNum - 1; i++)
        printf("%d, ", arr[i]);

    printf("%d]\n\n", arr[arrNum - 1]);
}

int main(int argc, char const *argv[])
{
    if (argc > 1)
    {
        int num = atoi(argv[1]);
        if (num > 0) omp_set_num_threads(num);
    }

    TEST_OMP(schedule(static))
    TEST_OMP(schedule(static, 3))
    TEST_OMP(schedule(dynamic))
    TEST_OMP(schedule(dynamic, 3))

    exit(EXIT_SUCCESS);
}
