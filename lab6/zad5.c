#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <omp.h>

#define DO_PRAGMA(x) _Pragma (#x)

#define TEST_CRITICAL(param)\
    sum = 0;\
    timeStart = omp_get_wtime();\
    printf("TEST: critical "#param"\n");\
    DO_PRAGMA(omp parallel for param)\
    for (size_t i = 0; i < 500; i++)\
    {\
        DO_PRAGMA(omp critical)\
        {\
            sum += number * number;\
        }\
    }\
    printf("Time: %lfs number: %d sum: %d\n", omp_get_wtime() - timeStart, number, sum);\

#define TEST_REDUCTION(param)\
    sum = 0;\
    timeStart = omp_get_wtime();\
    printf("TEST: reduction "#param"\n");\
    DO_PRAGMA(omp parallel for reduction(+:sum) param)\
    for (size_t i = 0; i < 500; i++)\
    {\
        sum += number * number;\
    }\
    printf("Time: %lfs number: %d sum: %d\n", omp_get_wtime() - timeStart, number, sum);\

#define TEST_LOCK(param)\
    omp_init_lock(&lock);\
    sum = 0;\
    timeStart = omp_get_wtime();\
    printf("TEST: lock "#param"\n");\
    DO_PRAGMA(omp parallel for param)\
    for (size_t i = 0; i < loop; i++)\
    {\
        omp_set_lock(&lock);\
        sum += number * number;\
        omp_unset_lock(&lock);\
    }\
    printf("Time: %lfs number: %d sum: %d\n", omp_get_wtime() - timeStart, number, sum);\
    omp_destroy_lock(&lock);\


int main(int argc, char const *argv[])
{
    srand(time(0));
    setbuf(stdout, NULL);

    const int number = argc > 1 ? atoi(argv[1]) : rand() % 100;
    const size_t loop = argc > 2 ? atoi(argv[2]) : 500;
    unsigned long long sum;
    double timeStart;
    omp_lock_t lock;

    TEST_CRITICAL(schedule(static))
    TEST_REDUCTION(schedule(static))
    TEST_LOCK(schedule(static))
    putchar('\n');

    TEST_CRITICAL(schedule(static, 3))
    TEST_REDUCTION(schedule(static, 3))
    TEST_LOCK(schedule(static, 3))
    putchar('\n');

    TEST_CRITICAL(schedule(dynamic))
    TEST_REDUCTION(schedule(dynamic))
    TEST_LOCK(schedule(dynamic))
    putchar('\n');

    TEST_CRITICAL(schedule(dynamic, 3))
    TEST_REDUCTION(schedule(dynamic, 3))
    TEST_LOCK(schedule(dynamic, 3))

    exit(EXIT_SUCCESS);
}
