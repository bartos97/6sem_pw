#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>
#include <unistd.h>
#include "../utils/pthread_utils.h"

#define WAIT_ARRAY_DIM 100
#define WAIT_ARRAY_SIZE WAIT_ARRAY_DIM * WAIT_ARRAY_DIM

#define DO_PRAGMA(x) _Pragma (#x)
#define TEST_OMP(param)\
    memset(a, 0, arraySize);\
    timeStart = omp_get_wtime();\
    printf("TEST: omp parallel for shared(a) "#param"\n");\
    DO_PRAGMA(omp parallel for shared(a) param)\
    for (size_t i = 0; i < arrayCount; i++)\
    {\
        a[i] = omp_get_thread_num();\
        busyWaiting();\
    }\
    printSummary(a, arrayCount, omp_get_wtime() - timeStart);\


static double a[WAIT_ARRAY_SIZE], b[WAIT_ARRAY_SIZE], c[WAIT_ARRAY_SIZE];
static inline void printSummary(int* arr, const size_t arrNum, double time);
static double busyWaiting();


int main(int argc, char const *argv[])
{
    if (argc > 1)
    {
        int num = atoi(argv[1]);
        if (num > 0) omp_set_num_threads(num);
    }
    const size_t arrayCount = argc > 2 ? atol(argv[2]) : 15;
    const size_t arraySize = sizeof(int) * arrayCount;

    int* a = (int*)malloc(arraySize);
    CUSTOM_ASSERT(a, "malloc failed!");
    double timeStart;

    TEST_OMP(schedule(static))
    TEST_OMP(schedule(static, 3))
    TEST_OMP(schedule(dynamic))
    TEST_OMP(schedule(dynamic, 3))

    free(a);
    exit(EXIT_SUCCESS);
}

static inline void printSummary(int* arr, const size_t arrNum, double time)
{
    #ifndef RELEASE_MODE
        printf("Time: %lfs; ", time);
        printf("threads sequence: [");

        for (size_t i = 0; i < arrNum - 1; i++)
            printf("%d, ", arr[i]);

        printf("%d]\n\n", arr[arrNum - 1]);
    #else
        printf("Time: %lfs\n", time);
    #endif
}


static double busyWaiting()
{
    const size_t N = WAIT_ARRAY_DIM;
    size_t i, j, k;

    for (i = 0; i < WAIT_ARRAY_SIZE; i++)
        a[i] = (rand() / (double)RAND_MAX) * i;
        
    for (i = 0; i < WAIT_ARRAY_SIZE; i++)
        b[i] = (rand() / (double)RAND_MAX) * (WAIT_ARRAY_SIZE - i);

    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            c[i + N * j] = 0.0;
            for (k = 0; k < N; k++)
            {
                c[i + N * j] += a[i + N * k] * b[k + N * j];
            }
        }
    }
    return (c[WAIT_ARRAY_SIZE - 1]);
}