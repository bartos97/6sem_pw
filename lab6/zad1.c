#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#define _STRINGIFY(s) #s
#define STRINGIFY(s) _STRINGIFY(s)


int main(int argc, char const *argv[])
{
    int a = 7;

#ifdef SHARED_ATTR_private
    printf("main: a=%d shared_attr=private\n", a);
    #pragma omp parallel for num_threads(7) private(a)
#endif
#ifdef SHARED_ATTR_firstprivate
    printf("main: a=%d shared_attr=firstprivate\n", a);
    #pragma omp parallel for num_threads(7) firstprivate(a)
#endif
#ifdef SHARED_ATTR_shared
    printf("main: a=%d shared_attr=shared\n", a);
    #pragma omp parallel for num_threads(7) shared(a)
#endif

    for (int i = 0; i < 10; i++)
    {
        printf("Thread %d a=%d\n", omp_get_thread_num(), a);
        a++;
    }

    printf("main a=%d\n", a);
    
    exit(EXIT_SUCCESS);
}
