#!/bin/bash

echo ./simd.exe "$@"
./simd.exe "$@"
echo
echo ./simd_parallel.exe "$@"
./simd_parallel.exe "$@"
echo
echo ./simd_sync.exe "$@"
./simd_sync.exe "$@"