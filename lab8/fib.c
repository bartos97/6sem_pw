#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <string.h>
#include "../utils/array_utils.h"
#include "../utils/errors.h"

#define RECURSION_CUTOFF 15

static size_t fib(size_t n);
static size_t _fib_normal(size_t n);
static size_t _fib_task(size_t n);
static size_t _fib_task_optimized(size_t n);

/**
 * 
 */
int main(int argc, char const *argv[])
{
    const size_t fibNum = argc > 1 ? atoi(argv[1]) : 10;
    double timeStart, timeEnd, timeTotal;
    size_t result = 0;

    timeStart = omp_get_wtime();
    result = fib(fibNum);
    timeEnd = omp_get_wtime();
    timeTotal = timeEnd - timeStart;
    printf("Result: %ld\n", result);
    printf("Total time: %.17gs\n", timeTotal);
    
    exit(EXIT_SUCCESS);
}


static size_t fib(size_t n)
{
#ifdef TASK
    return _fib_task(n);
#endif
#ifdef TASK_OPTIMIZED
    size_t retval;
    #pragma omp parallel
    #pragma omp single nowait
    retval = _fib_task_optimized(n);
    return retval;
#endif
    return _fib_normal(n);
}


static size_t _fib_normal(size_t n)
{
    if (n < 2) 
        return n;
    return _fib_normal(n - 1) + _fib_normal(n - 2);
}


static size_t _fib_task(size_t n)
{
    if (n < 2) 
        return n;

    size_t x, y;
    #pragma omp parallel
    #pragma omp single nowait
    {
        #pragma omp task shared(x)
        x = _fib_task(n - 1);
        #pragma omp task shared(y)
        y = _fib_task(n - 2);
        #pragma omp taskwait
        x += y;
    }
    return x;
}


static size_t _fib_task_optimized(size_t n)
{
    if (n < RECURSION_CUTOFF) 
        return _fib_normal(n);

    size_t x, y;

    #pragma omp task shared(x)
    x = _fib_task_optimized(n - 1);
    #pragma omp task shared(y)
    y = _fib_task_optimized(n - 2);
    #pragma omp taskwait
    x += y;

    return x;
}