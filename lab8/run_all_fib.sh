#!/bin/bash

echo ./fib.exe "$@"
./fib.exe "$@"
echo
echo ./fib_task.exe "$@"
./fib_task.exe "$@"
echo
echo ./fib_task_optimized.exe "$@"
./fib_task_optimized.exe "$@"