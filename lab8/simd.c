#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <string.h>
#include "../utils/array_utils.h"
#include "../utils/errors.h"

#define ARRAY_TYPE double

static ARRAY_TYPE dotProduct(ARRAY_TYPE* const vec1, ARRAY_TYPE* const vec2, const size_t length);
static void componentWiseMultiply(ARRAY_TYPE* const vec1, ARRAY_TYPE* const vec2, ARRAY_TYPE* const vecResult, const size_t length);

/**
 * vectorsLength, iterationsAmount, seed
 */
int main(int argc, char const *argv[])
{
    const size_t vectorsLength = argc > 1 ? atoi(argv[1]) : 5;
    const size_t iterationsAmount = argc > 2 ? atoi(argv[2]) : 100;
    const time_t seed = argc > 3 ? atoi(argv[3]) : time(NULL);
    const size_t maxValue = 10;

    double timeStart, timeEnd, timeTotal;
    ARRAY_TYPE result;

    ARRAY_TYPE* const vec1 = (ARRAY_TYPE*)malloc(vectorsLength * sizeof(ARRAY_TYPE));
    CUSTOM_ASSERT(vec1, "malloc failed");
    ARRAY_TYPE* const vec2 = (ARRAY_TYPE*)malloc(vectorsLength * sizeof(ARRAY_TYPE));
    CUSTOM_ASSERT(vec2, "malloc failed");
    ARRAY_TYPE* const vecResult = (ARRAY_TYPE*)calloc(vectorsLength, sizeof(ARRAY_TYPE));
    CUSTOM_ASSERT(vecResult, "malloc failed");

    fillArrayWithRandom(vec1, vectorsLength, maxValue, seed);
    fillArrayWithRandom(vec2, vectorsLength, maxValue, seed / 2);

#ifndef RELEASE_MODE
    printf("vector1: \n");
    printArray(vec1, vectorsLength);
    printf("vector2: \n");
    printArray(vec2, vectorsLength);
#endif

    result = 0;
    timeStart = omp_get_wtime();
    for (size_t i = 0; i < iterationsAmount; i++)
        result += dotProduct(vec1, vec2, vectorsLength);
    timeEnd = omp_get_wtime();
    timeTotal = timeEnd - timeStart;
    printf("Dot results sum: %"STRINGIFY(PRINT_SPCIFIER)"\n", result);
    printf("Dot total time: %.17gs\n", timeTotal);

    timeStart = omp_get_wtime();
    for (size_t i = 0; i < iterationsAmount; i++)
        componentWiseMultiply(vec1, vec2, vecResult, vectorsLength);
    timeEnd = omp_get_wtime();
    timeTotal = timeEnd - timeStart;
    printf("Component-wise total time: %.17gs\n", timeTotal);

#ifndef RELEASE_MODE
    printf("vectorResult: \n");
    printArray(vecResult, vectorsLength);
#endif

    free(vec1);
    free(vec2);
    free(vecResult);
    exit(EXIT_SUCCESS);
}


static ARRAY_TYPE dotProduct(ARRAY_TYPE* const vec1, ARRAY_TYPE* const vec2, const size_t length)
{
    ARRAY_TYPE sum = 0;

    #ifdef PARALLEL
        #pragma omp parallel for reduction(+:sum) schedule(static)
    #endif
    #ifdef SIMD
        #pragma omp simd reduction(+:sum)
    #endif
    for (size_t i = 0; i < length; i++)
        sum += vec1[i] * vec2[i];

    return sum;
}


static void componentWiseMultiply(ARRAY_TYPE* const vec1, ARRAY_TYPE* const vec2, ARRAY_TYPE* const vecResult, const size_t length)
{
    #ifdef PARALLEL
        #pragma omp parallel for schedule(static)
    #endif
    #ifdef SIMD
        #pragma omp simd
    #endif
    for (size_t i = 0; i < length; i++)
        vecResult[i] = vec1[i] * vec2[i];
}