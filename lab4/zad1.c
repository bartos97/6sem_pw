#include "../pthread_utils.h"

#define USE_MUTEX_LOCKS

static pthread_mutex_t g_mutex;
static size_t g_sharedVariable;
static const size_t g_numOfLoops = 1e6;

static void* client(void* threadArg);
static void* threadFunctDecrement(void* threadArg);

int main(int argc, char const *argv[])
{
    pthread_t threadID1, threadID2;
    int retCode;

    pthread_mutex_init(&g_mutex, NULL);

    retCode = pthread_create(&threadID1, NULL, client, NULL);
    checkError(retCode, "create increment");

    retCode = pthread_create(&threadID2, NULL, threadFunctDecrement, NULL);
    checkError(retCode, "create decrement");

    waitForThread(threadID1);
    waitForThread(threadID2);

    printf("Value in global variable: %ld\n", g_sharedVariable);

    pthread_mutex_destroy(&g_mutex);
    exit(EXIT_SUCCESS);
}

static void* client(void* threadArg)
{
    printf("Incrementing thread created\n");

    for (size_t i = 0; i < g_numOfLoops; i++)
    {
    #ifdef USE_MUTEX_LOCKS
        pthread_mutex_lock(&g_mutex);
    #endif

        g_sharedVariable++;

    #ifdef USE_MUTEX_LOCKS
        pthread_mutex_unlock(&g_mutex);
    #endif
    }
    
    return NULL;
}

static void* threadFunctDecrement(void* threadArg)
{
    printf("Decrementing thread created\n");

    for (size_t i = 0; i < g_numOfLoops; i++)
    {
    #ifdef USE_MUTEX_LOCKS
        pthread_mutex_lock(&g_mutex);
    #endif

        g_sharedVariable--;

    #ifdef USE_MUTEX_LOCKS
        pthread_mutex_unlock(&g_mutex);
    #endif
    }
    
    return NULL;
}