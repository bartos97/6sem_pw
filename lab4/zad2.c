#define SILENT_JOIN
#include "../pthread_utils.h"
#include <time.h>
#include <unistd.h>

#define DEFAULT_CLIENTS 4
#define MAX_CLIENTS 16
#define DEFAULT_MUGS 3
#define MAX_MUGS 15
#define DEFAULT_DRINK_TIME 10
#define MAX_DRINK_TIME 60

static size_t g_numOfClients = DEFAULT_CLIENTS;
static size_t g_numOfBeerMugs = DEFAULT_MUGS;
static size_t g_maxDrinkTime = DEFAULT_DRINK_TIME; //in seconds
static const size_t g_mugsPerClient = 2;

static pthread_mutex_t g_mugsMutexes[MAX_MUGS];

static void* clientFunction(void* threadArg);

// zad2.exe g_numOfClients g_numOfBeerMugs g_maxDrinkTime
int main(int argc, char const *argv[])
{
    setbuf(stdout, NULL);
    pthread_t clientThreadIDs[MAX_CLIENTS];

    if (argc > 1) 
    {
        int tmp = atoi(argv[1]);
        g_numOfClients = tmp > 0 && tmp <= MAX_CLIENTS ? tmp : DEFAULT_CLIENTS;

        if (argc > 2)
        {
            tmp = atoi(argv[2]);
            g_numOfBeerMugs = tmp > 0 && tmp <= MAX_MUGS ? tmp : DEFAULT_MUGS;
        }

        if (argc > 3)
        {
            tmp = atoi(argv[3]);
            g_maxDrinkTime = tmp > 0 && tmp <= MAX_DRINK_TIME ? tmp : DEFAULT_DRINK_TIME;
        }
    }

    for (size_t i = 0; i < g_numOfBeerMugs; i++)
    {
        pthread_mutex_init(&g_mugsMutexes[i], NULL);
    }    

    for (size_t i = 0; i < g_numOfClients; i++)
    {
        checkError(pthread_create(&clientThreadIDs[i], NULL, clientFunction, (void*)i), "thread create");
    }
    
    for (size_t i = 0; i < g_numOfClients; i++)
    {
        waitForThread(clientThreadIDs[i]);
    }
    printf("Closing the bar\n");
    exit(EXIT_SUCCESS);
}

static void* clientFunction(void* threadArg)
{
    size_t fakeID = (size_t)threadArg;
    clock_t timeStart, timeEnd;
    unsigned int sleepTime, mugsTaken = 0;
    int finished = 0;

    printf("Client #%ld enters the bar\n", fakeID);
    timeStart = clock();
    
    while (!finished)
    {
        for (size_t i = 0; i < g_numOfBeerMugs; i++)
        {
            if (pthread_mutex_trylock(&g_mugsMutexes[i]) == 0)
            {
                mugsTaken++;
                sleepTime = (unsigned int)(rand() % g_maxDrinkTime);
                printf("Client #%ld grabs beer in mug #%ld\n", fakeID, i);
                sleep(sleepTime);
                printf("Client #%ld passes beer in after %ds in mug #%ld\n", fakeID, sleepTime, i);
                pthread_mutex_unlock(&g_mugsMutexes[i]);
            }

            if (mugsTaken >= g_mugsPerClient)
            {
                finished = 1;
                break;
            }
        }            
    }

    timeEnd = clock();
    printf("Client #%ld leaves the bar after %lfs\n", fakeID, (double)(timeEnd - timeStart) / CLOCKS_PER_SEC);
    pthread_exit(NULL);
}