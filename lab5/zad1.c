#include <sys/sysinfo.h>
#include <time.h>
#include <stdatomic.h>

#define ARRAY_TYPE int
#define SILENT_JOIN
#include "../utils/pthread_utils.h"
#include "../utils/array_utils.h"

struct MergeArgument
{
    ARRAY_TYPE* const arraySource;
    ARRAY_TYPE* const arrayBuffer;
    const unsigned int indexStart;
    const unsigned int indexMiddle;
    const unsigned int indexPastEnd;
};

struct MergeArgumentDisjointArray
{
    ARRAY_TYPE* const arraySource;
    ARRAY_TYPE* const arrayBuffer;
    unsigned int leftArrayStart;
    unsigned int leftArrayPastEnd;
    unsigned int rightArrayStart;
    unsigned int rightArrayPastEnd;
    unsigned int sourceArrayStart;
    unsigned int sourceArrayPastEnd;
};


static void mergeSort(ARRAY_TYPE* const arraySource, const unsigned int arrayCount);
static void* mergeSortSplit(void* mergeArgument);
static void* mergeSortMerge(void* mergeArgument);
static void* mergeSortRecursiveMerge(void* mergeArgument);
static unsigned int mergeSortBinarySearch(ARRAY_TYPE value, ARRAY_TYPE* sourceArray, unsigned int indexStart, unsigned int indexPastEnd);

static int g_nprocs;
// static volatile atomic_int g_threadCount;
static unsigned int g_arrayCount;


/**
 * @param 1. arrayCount: number of elements in array; default: 10
 * @param 2. arrayMaxValue:  maximum random value to be in array; default: RAND_MAX (pass 0 if you want that)
 * @param 3. seed: seed for random number generator (if ya want same array every run); default: current time
 */
int main(int argc, char const *argv[])
{
#ifndef NO_FILE
    FILE* file = fopen("result.dat", "w");
    CUSTOM_ASSERT(file, "Error when opening / creating file\n");
#endif

    struct timespec timeStart, timeEnd;
    const unsigned int arrayCount = argc > 1 ? atoi(argv[1]) : 10;
    const unsigned int arrayMaxValue = argc > 2 ? atoi(argv[2]) : 0;
    const unsigned int seed = argc > 3 ? atoi(argv[3]) : time(NULL);

    g_nprocs = get_nprocs();
    g_arrayCount = arrayCount;

    ARRAY_TYPE* const arraySource = (ARRAY_TYPE*)malloc(arrayCount * sizeof(ARRAY_TYPE));
    CUSTOM_ASSERT(arraySource, "malloc failed");
    fillArrayWithRandom(arraySource, arrayCount, arrayMaxValue, seed);

#ifndef RELEASE_MODE
    printArray(arraySource, arrayCount);
#endif

    clock_gettime(CLOCK_MONOTONIC, &timeStart);
    mergeSort(arraySource, arrayCount);
    clock_gettime(CLOCK_MONOTONIC, &timeEnd);

#ifndef RELEASE_MODE
    printArray(arraySource, arrayCount);
#endif

    double timeTotal = timeEnd.tv_sec - timeStart.tv_sec;
    timeTotal += (timeEnd.tv_nsec - timeStart.tv_nsec) / 1e9;
    printf("Total sorting time (with buffer allocation): %.17gs\n", timeTotal);

#ifndef NO_FILE
    fprintf(file, "Total sorting time (with buffer allocation): %.17gs\n", timeTotal);
    printArrayToFile(arraySource, arrayCount, file);
    fclose(file);
#endif
    
    free(arraySource);
    exit(EXIT_SUCCESS);
}


static void mergeSort(ARRAY_TYPE* const arraySource, const unsigned int arrayCount)
{
    ARRAY_TYPE* const arrayBuffer = (ARRAY_TYPE*)malloc(arrayCount * sizeof(size_t));
    CUSTOM_ASSERT(arrayBuffer, "malloc failed");
    copyArray(arraySource, arrayCount, arrayBuffer, arrayCount);

    struct MergeArgument arg = { 
        .arraySource = arraySource, 
        .arrayBuffer = arrayBuffer, 
        .indexStart = 0, 
        .indexMiddle = (arrayCount - 1) / 2,
        .indexPastEnd = arrayCount
    };
    mergeSortSplit(&arg);
    // printf("%d\n", atomic_load(&g_threadCount));

    free(arrayBuffer);
}


static void* mergeSortSplit(void* mergeArgument)
{
    static volatile atomic_int threadCount;

    CUSTOM_ASSERT(mergeArgument, "Argument not specified");
    struct MergeArgument* arg = (struct MergeArgument*)mergeArgument;

    if (arg->indexPastEnd - arg->indexStart < 2) 
        return NULL;

    struct MergeArgument argLeft = {
        .arraySource = arg->arrayBuffer, 
        .arrayBuffer = arg->arraySource, 
        .indexStart = arg->indexStart,
        .indexMiddle = (arg->indexMiddle + arg->indexStart) / 2,
        .indexPastEnd = arg->indexMiddle
    };
    struct MergeArgument argRight = {
        .arraySource = arg->arrayBuffer, 
        .arrayBuffer = arg->arraySource, 
        .indexStart = arg->indexMiddle,
        .indexMiddle = (arg->indexPastEnd + arg->indexMiddle)  / 2,
        .indexPastEnd = arg->indexPastEnd
    };
    struct MergeArgumentDisjointArray argMerge = {
        .arraySource = arg->arraySource,
        .arrayBuffer = arg->arrayBuffer,
        .leftArrayStart = arg->indexStart,
        .leftArrayPastEnd = arg->indexMiddle,
        .rightArrayStart = arg->indexMiddle,
        .rightArrayPastEnd = arg->indexPastEnd,
        .sourceArrayStart = arg->indexStart,
        .sourceArrayPastEnd = arg->indexPastEnd
    };

#ifdef NO_PARALLEL
    mergeSortSplit(&argLeft);
    mergeSortSplit(&argRight);

    #ifdef RECURSIVE_MERGE
        mergeSortRecursiveMerge(&argMerge);
    #else
        mergeSortMerge(&argMerge);
    #endif
#else
    if (atomic_load(&threadCount) <= g_nprocs)
    {
        pthread_t threadLeft, threadRight;
        checkError(pthread_create(&threadLeft, NULL, mergeSortSplit, &argLeft), "thread create");
        atomic_fetch_add(&threadCount, 1);

        mergeSortSplit(&argRight);
        waitForThread(threadLeft);
    }
    else
    {
        mergeSortSplit(&argLeft);
        mergeSortSplit(&argRight);
    }
    
    #ifdef RECURSIVE_MERGE
        mergeSortRecursiveMerge(&argMerge);
    #else
        mergeSortMerge(&argMerge);
    #endif
#endif

    return NULL;
}


static void* mergeSortMerge(void* mergeArgument)
{
    CUSTOM_ASSERT(mergeArgument, "Argument not specified");
    struct MergeArgumentDisjointArray* arg = (struct MergeArgumentDisjointArray*)mergeArgument;

    unsigned int leftArrayIndex = arg->leftArrayStart;
    unsigned int rightArrayIndex = arg->rightArrayStart;

    for (unsigned int i = arg->sourceArrayStart; i < arg->sourceArrayPastEnd; i++)
    {
        if (
            leftArrayIndex < arg->leftArrayPastEnd 
            && (rightArrayIndex >= arg->rightArrayPastEnd || arg->arrayBuffer[leftArrayIndex] <= arg->arrayBuffer[rightArrayIndex])
        ) {
            arg->arraySource[i] = arg->arrayBuffer[leftArrayIndex];
            leftArrayIndex++;
        }
        else
        {
            arg->arraySource[i] = arg->arrayBuffer[rightArrayIndex];
            rightArrayIndex++;
        }
    }

    return NULL;
}


static void* mergeSortRecursiveMerge(void* mergeArgument)
{
    CUSTOM_ASSERT(mergeArgument, "Argument not specified");
    struct MergeArgumentDisjointArray* arg = (struct MergeArgumentDisjointArray*)mergeArgument;

    // swap if smaller
    if (arg->leftArrayPastEnd - arg->leftArrayStart < arg->rightArrayPastEnd - arg->rightArrayStart)
    {
        unsigned int tmpLeftStart = arg->leftArrayStart;
        unsigned int tmpLeftEnd = arg->leftArrayPastEnd;
        arg->leftArrayStart = arg->rightArrayStart;
        arg->leftArrayPastEnd = arg->rightArrayPastEnd;
        arg->rightArrayStart = tmpLeftStart;
        arg->rightArrayPastEnd = tmpLeftEnd;
    }

    int leftLength = arg->leftArrayPastEnd - arg->leftArrayStart;
    if (leftLength <= 0)
        return NULL;

    unsigned int leftArrayMiddle = (arg->leftArrayStart + arg->leftArrayPastEnd) / 2;
    unsigned int rightArrayPivot 
        = mergeSortBinarySearch(arg->arrayBuffer[leftArrayMiddle], arg->arrayBuffer, arg->rightArrayStart, arg->rightArrayPastEnd);
    unsigned int sourceArrayPivot = arg->sourceArrayStart + (leftArrayMiddle - arg->leftArrayStart) + (rightArrayPivot - arg->rightArrayStart);

    arg->arraySource[sourceArrayPivot] = arg->arrayBuffer[leftArrayMiddle];

    struct MergeArgumentDisjointArray argLeft = {
        .arraySource = arg->arraySource,
        .arrayBuffer = arg->arrayBuffer,
        .leftArrayStart = arg->leftArrayStart,
        .leftArrayPastEnd = leftArrayMiddle,
        .rightArrayStart = arg->rightArrayStart,
        .rightArrayPastEnd = rightArrayPivot,
        .sourceArrayStart = arg->sourceArrayStart,
        .sourceArrayPastEnd = sourceArrayPivot
    };
    struct MergeArgumentDisjointArray argRight = {
        .arraySource = arg->arraySource,
        .arrayBuffer = arg->arrayBuffer,
        .leftArrayStart = leftArrayMiddle + 1,
        .leftArrayPastEnd = arg->leftArrayPastEnd,
        .rightArrayStart = rightArrayPivot,
        .rightArrayPastEnd = arg->rightArrayPastEnd,
        .sourceArrayStart = sourceArrayPivot + 1,
        .sourceArrayPastEnd = arg->sourceArrayPastEnd
    };

#ifdef NO_PARALLEL
    mergeSortRecursiveMerge(&argLeft);
    mergeSortRecursiveMerge(&argRight);
#else
    pthread_t threadLeft, threadRight;
    int doSplit = leftLength > g_arrayCount / 500;

    if (doSplit && pthread_create(&threadLeft, NULL, mergeSortRecursiveMerge, &argLeft) == 0)
    {
        // atomic_fetch_add(&g_threadCount, 1);
        mergeSortRecursiveMerge(&argRight);
        waitForThread(threadLeft);
    }
    else
    {
        mergeSortRecursiveMerge(&argLeft);
        mergeSortRecursiveMerge(&argRight);
    }
#endif

    return NULL;
}


static unsigned int mergeSortBinarySearch(ARRAY_TYPE value, ARRAY_TYPE* sourceArray, unsigned int indexStart, unsigned int indexPastEnd)
{
    int middle;
    while (indexStart < indexPastEnd)
    {
        middle = (indexStart + indexPastEnd) / 2;
        if (value > sourceArray[middle])
        {
            indexStart = middle + 1;
        }
        else if (value < sourceArray[middle])
        {
            indexPastEnd = middle;
        }
        else return middle;
    }
    return indexStart;
}