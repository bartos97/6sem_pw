#include <sys/sysinfo.h>
#include <time.h>
#include <math.h>
#include <stdatomic.h>

#define SILENT_JOIN
#include "../utils/pthread_utils.h"

// struct ThreadArgument
// {
//     size_t prime;
//     volatile atomic_bool* array;
//     size_t arrayCount;
// };

static void sieve(volatile atomic_bool* array, size_t arrayCount);
static void* crossMultiplesOut(void* threadArgument);
static void printArray(volatile atomic_bool* array, size_t arrayCount);
static void printPrimes(volatile atomic_bool* array, size_t arrayCount);
static void printPrimesToFile(FILE* file, volatile atomic_bool* array, size_t arrayCount);

static size_t g_arrayCount;
static volatile atomic_bool*  g_arraySource;
static pthread_t* g_threadIDs;
static size_t g_maxThreads;


int main(int argc, char const *argv[])
{
#ifndef NO_FILE
    FILE* file = fopen("prime_numbers.dat", "w");
    CUSTOM_ASSERT(file, "Error when opening / creating file\n");
#endif

    struct timespec timeStart, timeEnd;

    g_arrayCount = argc > 1 ? atoi(argv[1]) : 50;
    g_arrayCount++;

    g_arraySource = (volatile atomic_bool*)malloc(g_arrayCount * sizeof(volatile atomic_bool));
    CUSTOM_ASSERT(g_arraySource, "malloc failed");

    g_maxThreads = (size_t)sqrt(g_arrayCount);
    g_threadIDs = (pthread_t*)malloc(g_maxThreads * sizeof(pthread_t));
    CUSTOM_ASSERT(g_threadIDs, "malloc failed");
 
    for (size_t i = 0; i < g_arrayCount; i++)
        atomic_init(&g_arraySource[i], 1);

    clock_gettime(CLOCK_MONOTONIC, &timeStart);
    sieve(g_arraySource, g_arrayCount);
    clock_gettime(CLOCK_MONOTONIC, &timeEnd);

// #ifndef RELEASE_MODE
//     printPrimes(g_arraySource, g_arrayCount);
// #endif

    double timeTotal = timeEnd.tv_sec - timeStart.tv_sec;
    timeTotal += (timeEnd.tv_nsec - timeStart.tv_nsec) / 1e9;
    printf("Total time: %.17gs\n", timeTotal);
    
#ifndef NO_FILE
    fprintf(file, "Total time: %.17gs\n", timeTotal);
    printPrimesToFile(file, g_arraySource, g_arrayCount);
    fclose(file);
#endif

    free((void*)g_arraySource);
    exit(EXIT_SUCCESS);
}


static void sieve(volatile atomic_bool* array, size_t arrayCount)
{
    atomic_uint threadCount;
    size_t basePrimesCount = 0;

    size_t* const basePrimes = (size_t*)malloc(g_maxThreads * sizeof(size_t));
    CUSTOM_ASSERT(basePrimes, "malloc failed");
    atomic_init(&threadCount, 0);
    
    //find, save and cross out primes up to sqrt(arrayCount)
    for (size_t i = 2; i * i <= arrayCount; i++)
    {
        if (atomic_load(&array[i]) == 1) //is prime; that is, not crossed out yet
        {
            basePrimes[basePrimesCount++] = i;
            for (size_t j = i * i; j * j <= arrayCount; j += i)
                atomic_store(&array[j], 0);
        }
    }

    // cross rest of multiples out, in parallel
    CUSTOM_LOG("base primes: %d", basePrimesCount);
    for (size_t i = 0; i < basePrimesCount; i++)
    {
    #ifdef NO_PARALLEL
        crossMultiplesOut((void*)basePrimes[i]);
    #else
        unsigned int id = atomic_fetch_add(&threadCount, 1);
        checkError(pthread_create(&g_threadIDs[id], NULL, crossMultiplesOut, (void*)basePrimes[i]), "thread create");
    #endif
    }
    
    CUSTOM_LOG("thread count: %d", atomic_load(&threadCount));
    for (size_t i = 0; i < atomic_load(&threadCount); i++)
        waitForThread(g_threadIDs[i]);
    
    free(basePrimes);
}


static void* crossMultiplesOut(void* threadArgument)
{
    size_t prime = (size_t)threadArgument;

    for (size_t j = prime * prime; j <= g_arrayCount; j += prime)
    {
        atomic_store(&(g_arraySource[j]), 0);
    }
    
    return NULL;
}

static void printArray(volatile atomic_bool* array, size_t arrayCount)
{
    for (size_t i = 0; i < arrayCount; i++)
        printf("%d ", atomic_load(&array[i]));
    putchar('\n');
}

static void printPrimes(volatile atomic_bool* array, size_t arrayCount)
{
    for (size_t i = 2; i < arrayCount; i++)
    {
        if (atomic_load(&array[i]) == 1) 
            printf("%d ", i);
    }
    putchar('\n');
}

static void printPrimesToFile(FILE* file, volatile atomic_bool* array, size_t arrayCount)
{
    for (size_t i = 2; i < arrayCount; i++)
    {
        if (atomic_load(&array[i]) == 1) 
            fprintf(file, "%d\n", i);
    }
}