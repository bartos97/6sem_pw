#include <mpi.h>
#include <iostream>
#include <cmath>

static inline size_t getNthOddNumber(size_t n)
{
    size_t retval = 1;
    for (size_t i = 1; i < n; i++)
    {
        retval += 2;
    }
    return retval;
}

static double calcPartialSum(const size_t startIndex, const size_t pastEndIndex)
{
    size_t denominator = getNthOddNumber(startIndex);
    double sum = 0;

    for (size_t i = startIndex; i < pastEndIndex; i++)
    {
        const int sign = (i % 2) == 0 ? 1 : -1;
        sum += sign * (1.0 / denominator);
        denominator += 2;
    }
    
    return sum;
}

int main(int argc, char *argv[])
{
    int numOfProcesses, selfRank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numOfProcesses);
    MPI_Comm_rank(MPI_COMM_WORLD, &selfRank);

    constexpr int ROOT_ID = 0;
    const size_t sumElemsAmount = argc > 1 ? atol(argv[1]) > 0l ? atol(argv[1]) : 10 : 10;

    const double timeStart = MPI_Wtime();

    const size_t perRankChunkCount = sumElemsAmount / numOfProcesses;
    const size_t leftoverChunkCount = sumElemsAmount % numOfProcesses;
    size_t startTranslation = 0;
    size_t endTranslation = 0;
    if (leftoverChunkCount > 0)
    {
        bool anythingToDispense = long(leftoverChunkCount - selfRank) > 0;
        startTranslation = anythingToDispense ? selfRank : leftoverChunkCount;
        endTranslation = anythingToDispense ? 1 : 0;
    }
    const size_t startIndex = selfRank * perRankChunkCount + startTranslation;
    const size_t pastEndIndex = startIndex + perRankChunkCount + endTranslation;

    double localSum = 0.0;
    double globalSum = 0.0;
    
    std:: cout << "[Rank #" << selfRank << "] counts in [" << startIndex << ',' << pastEndIndex << ')' << std::endl;
    localSum += calcPartialSum(startIndex, pastEndIndex);

    MPI_Reduce(&localSum, &globalSum, 1, MPI_DOUBLE, MPI_SUM, ROOT_ID, MPI_COMM_WORLD);
    
    const double endTime = MPI_Wtime();
    if (selfRank == ROOT_ID)
    {
        double absErr = std::abs(globalSum - M_PI_4);
        std::cout
            << "Amount of sum elements: " << sumElemsAmount << '\n'
            << "Result: " << globalSum << " (PI: " << globalSum * 4 << ")\n"
            << "Absolute error: " << absErr << '\n'
            << "Relative error: " << (absErr / M_PI_4) * 100.0 << "% \n"
            << "Total time: " << endTime - timeStart << 's' << std::endl;
    }

    MPI_Finalize();
    return 0;
}