#include <mpi.h>
#include <iostream>
#include <cmath>

static inline double calcPartialSum(size_t startIndex, size_t pastEndIndex)
{
    startIndex = startIndex == 0 ? 1 : startIndex;
    double sum = 0.0;
    for (size_t i = startIndex; i < pastEndIndex; i++)
    {
        sum += 1.0 / i;
    }
    return sum;
}

int main(int argc, char *argv[])
{
    constexpr double GAMMA = 0.5772156649;
    constexpr int ROOT_ID = 0;
    constexpr int NUM_OF_PROC = 2;

    const size_t sumElemsAmount = argc > 1 ? atol(argv[1]) > 0l ? atol(argv[1]) : 10 : 10;

    int numOfProcesses, selfRank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numOfProcesses);
    MPI_Comm_rank(MPI_COMM_WORLD, &selfRank);

    if (numOfProcesses != NUM_OF_PROC)
    {
        std::cerr << "Invalid processes amount, 2 required" << std::endl;
        exit(EXIT_FAILURE);
    }

    const double timeStart = MPI_Wtime();

    const size_t perRankChunkCount = sumElemsAmount / NUM_OF_PROC;
    const size_t leftoverChunkCount = sumElemsAmount % NUM_OF_PROC;
    size_t startTranslation = 0;
    size_t endTranslation = 0;
    if (leftoverChunkCount > 0)
    {
        bool anythingToDispense = long(leftoverChunkCount - selfRank) > 0;
        startTranslation = anythingToDispense ? selfRank : leftoverChunkCount;
        endTranslation = anythingToDispense ? 1 : 0;
    }
    const size_t startIndex = selfRank * perRankChunkCount + startTranslation;
    const size_t pastEndIndex = startIndex + perRankChunkCount + endTranslation;

    double localSum = 0.0;
    double globalSum = 0.0;
    
    std:: cout << "[Rank #" << selfRank << "] counts in [" << startIndex << ',' << pastEndIndex << ')' << std::endl;
    localSum += calcPartialSum(startIndex, pastEndIndex);

    MPI_Reduce(&localSum, &globalSum, 1, MPI_DOUBLE, MPI_SUM, ROOT_ID, MPI_COMM_WORLD);
    globalSum -= std::log(sumElemsAmount);
    
    const double endTime = MPI_Wtime();
    if (selfRank == ROOT_ID)
    {
        double absErr = std::abs(globalSum - GAMMA);
        std::cout
            << "Amount of sum elements: " << sumElemsAmount << '\n'
            << "Result: " << globalSum << '\n'
            << "Absolute error: " << absErr << '\n'
            << "Relative error: " << (absErr / GAMMA) * 100.0 << "% \n"
            << "Total time: " << endTime - timeStart << 's' << std::endl;
    }

    MPI_Finalize();
    return 0;
}